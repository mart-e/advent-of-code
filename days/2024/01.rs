use advent_of_code::get_lines;

use regex::Regex;

fn part1(lines: &str) -> usize {
    let Ok(re) = Regex::new(r"(?<id1>\d*)   (?<id2>\d*)") else {
        panic!("bad regex")
    };

    let (mut left, mut right): (Vec<usize>, Vec<usize>) = lines
        .lines()
        .filter_map(|line| {
            re.captures(line)
                .map(|capt| {
                    let id1: usize = capt["id1"].parse().expect("id1 is not a numbre");
                    let id2: usize = capt["id2"].parse().expect("id2 is not a numbre");
                    (id1, id2)
                })
                .inspect(|(x, y)| {
                    println!("{x} - {y}");
                })
        })
        .unzip();
    left.sort();
    right.sort();
    left.into_iter()
        .zip(right)
        .map(|(l, r)| l.abs_diff(r))
        .sum()
}

fn part2(lines: &str) -> usize {
    let Ok(re) = Regex::new(r"(?<id1>\d*)   (?<id2>\d*)") else {
        panic!("bad regex")
    };

    let (left, right): (Vec<usize>, Vec<usize>) = lines
        .lines()
        .filter_map(|line| {
            re.captures(line)
                .map(|capt| {
                    let id1: usize = capt["id1"].parse().expect("id1 is not a numbre");
                    let id2: usize = capt["id2"].parse().expect("id2 is not a numbre");
                    (id1, id2)
                })
                .inspect(|(x, y)| {
                    println!("{x} - {y}");
                })
        })
        .unzip();
    left.into_iter()
        .map(|l| right.iter().filter(|&&r| r == l).count() * l)
        .sum()
}

fn main() {
    let (filename, lines) = get_lines();

    let p1 = part1(&lines);
    println!("Part 1 result: {p1}");
    let p2 = part2(&lines);
    println!("Part 2 result: {p2}");

    if filename.contains("example") {
        assert_eq!(p1, 11);
        assert_eq!(p2, 31);
    } else {
        assert_eq!(p1, 936063);
        assert_eq!(p2, 893);
    }

    println!("The end!");
}

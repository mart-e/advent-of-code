use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}
enum Shape {
    Rock,
    Paper,
    Scissors,
}

const WIN: u32 = 6;
const DRAW: u32 = 3;
const LOSE: u32 = 0;

fn round(me: Shape, them: Shape) -> u32 {
    match me {
        Shape::Paper => match them {
            Shape::Paper => DRAW,
            Shape::Rock => WIN,
            Shape::Scissors => LOSE,
        },
        Shape::Rock => match them {
            Shape::Paper => LOSE,
            Shape::Rock => DRAW,
            Shape::Scissors => WIN,
        },
        Shape::Scissors => match them {
            Shape::Paper => WIN,
            Shape::Rock => LOSE,
            Shape::Scissors => DRAW,
        },
    }
}

fn find_winner(line: String) -> u32 {
    let mut shapes: Vec<&str> = line.split(' ').collect();
    let first = shapes.remove(0);
    let them: Shape;
    if first == "A" {
        them = Shape::Rock;
    } else if first == "B" {
        them = Shape::Paper;
    } else {
        them = Shape::Scissors;
    }
    let me = shapes.remove(0);
    if me == "X" {
        round(Shape::Rock, them) + 1
    } else if me == "Y" {
        round(Shape::Paper, them) + 2
    } else {
        round(Shape::Scissors, them) + 3
    }
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2022")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: u32 = 0;
    for line_ in reader.lines() {
        sum += find_winner(line_.unwrap());
    }
    println!("Sum of all of the calibration values: {}", sum);
}

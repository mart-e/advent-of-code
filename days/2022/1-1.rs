use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

fn main() {
    let args = App::new("parse-log")
        .version("0.1")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);
    let mut calories: i32 = 0;
    let mut all_calories: i32 = 0;
    for line_ in reader.lines() {
        let line = line_.unwrap();
        if line.trim().is_empty() {
            if calories > max_calories {
                println!("{calories} > {max_calories}");
                max_calories = calories;
            }
            calories = 0;
        } else {
            calories += line.parse::<i32>().unwrap();
        }
    }
    println!("{max_calories}");
}

use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

fn split_line(line: &str) -> (&str, &str) {
    line.split_at(line.len() / 2)
}

fn find_common(left: &str, right: &str) -> char {
    for c in left.chars() {
        if right.contains(c) {
            return c;
        }
    }
    panic!("no common letter found");
}

const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn value(c: char) -> usize {
    ALPHABET.find(c).unwrap() + 1
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2022")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: usize = 0;
    for line_ in reader.lines() {
        let line = &line_.unwrap();
        let (left, right) = split_line(line);
        let common: char = find_common(left, right);
        println!("common: {} (value {})", common, value(common));
        sum += value(common);
    }

    println!("Total power: {}", sum);
}

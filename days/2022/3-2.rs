use std::fs;
use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

fn value(c: char) -> usize {
    ALPHABET.find(c).unwrap() + 1
}

fn split_lines(lines: &str) -> Vec<(&str, &str, &str)> {
    let mut res: Vec<(&str, &str, &str)> = vec![];

    let mut all = lines.lines();
    loop {
        let Some(first) = all.next() else {
            break;
        };
        let Some(second) = all.next() else {
            panic!("missing second element");
        };
        let Some(third) = all.next() else {
            panic!("missing third element");
        };
        res.push((first, second, third));
    }
    res
}

fn find_common(one: &str, two: &str, three: &str) -> char {
    for c in one.chars() {
        if two.contains(c) & three.contains(c) {
            return c;
        }
    }
    panic!("no common letter found");
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2022")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename);
    };

    let mut sum: usize = 0;
    for (one, two, three) in split_lines(&lines) {
        let common: char = find_common(one, two, three);
        println!("common: {} (value {})", common, value(common));
        sum += value(common);
    }

    println!("Total power: {}", sum);
}

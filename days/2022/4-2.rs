use advent_of_code::get_lines;

use regex::Regex;

fn part1(lines: &str) -> usize {
    let Ok(re) = Regex::new(r"(?<s1>\d*)-(?<e1>\d*),(?<s2>\d*)-(?<e2>\d*)") else {
        panic!("bad regex")
    };

    lines
        .lines()
        .filter_map(|line| {
            re.captures(line).filter(|capt| {
                let s1: usize = capt["s1"].parse().expect("s1 is not a numbre");
                let s2: usize = capt["s2"].parse().expect("s2 is not a numbre");
                let e1: usize = capt["e1"].parse().expect("e1 is not a numbre");
                let e2: usize = capt["e2"].parse().expect("e2 is not a numbre");
                (s1 <= s2 && e1 >= e2) || (s1 >= s2 && e1 <= e2)
            })
            //         .inspect(|_capt| {
            //             println!("{line}");
            //         })
        })
        .collect::<Vec<_>>()
        .len()
}

fn part2(lines: &str) -> usize {
    let Ok(re) = Regex::new(r"(?<s1>\d*)-(?<e1>\d*),(?<s2>\d*)-(?<e2>\d*)") else {
        panic!("bad regex")
    };

    lines
        .lines()
        .filter_map(|line| {
            re.captures(line).filter(|capt| {
                let s1: usize = capt["s1"].parse().expect("s1 is not a numbre");
                let s2: usize = capt["s2"].parse().expect("s2 is not a numbre");
                let e1: usize = capt["e1"].parse().expect("e1 is not a numbre");
                let e2: usize = capt["e2"].parse().expect("e2 is not a numbre");
                (s2 <= s1 && s1 <= e2) || (s1 <= s2 && s2 <= e1)
            })
            // .inspect(|_capt| {
            //     println!("{line}");
            // })
        })
        .collect::<Vec<_>>()
        .len()
}

fn main() {
    let (filename, lines) = get_lines();

    let p1 = part1(&lines);
    println!("Part 1 result: {p1}");
    let p2 = part2(&lines);
    println!("Part 2 result: {p2}");

    if filename.contains("example") {
        assert_eq!(p1, 2);
        assert_eq!(p2, 4);
    } else {
        assert_eq!(p1, 582);
        assert_eq!(p2, 893);
    }

    println!("The end!");
}

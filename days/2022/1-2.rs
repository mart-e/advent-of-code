use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

fn main() {
    let args = App::new("parse-log")
        .version("0.1")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);
    let mut all_calories: Vec<i32> = vec![];
    for line_ in reader.lines() {
        let line = line_.unwrap();
        if line.trim().is_empty() {
            all_calories.push(0);
        } else {
            if let Some(last) = all_calories.last_mut() {
                *last += line.parse::<i32>().unwrap();
            }
        }
    }
    all_calories.sort();
    let l: usize = all_calories.len();
    let total: i32 = all_calories[l - 1] + all_calories[l - 2] + all_calories[l - 3];
    println!("{total}");
}

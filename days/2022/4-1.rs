use advent_of_code::get_lines;

use regex::Regex;

fn main() {
    let (_filename, lines) = get_lines();

    let Ok(re) = Regex::new(r"(?<s1>\d*)-(?<e1>\d*),(?<s2>\d*)-(?<e2>\d*)") else {
        panic!("bad regex")
    };

    let tot = lines
        .lines()
        .filter_map(|line| {
            re.captures(line)
                .filter(|capt| {
                    let s1: usize = capt["s1"].parse().expect("s1 is not a numbre");
                    let s2: usize = capt["s2"].parse().expect("s2 is not a numbre");
                    let e1: usize = capt["e1"].parse().expect("e1 is not a numbre");
                    let e2: usize = capt["e2"].parse().expect("e2 is not a numbre");
                    (s1 <= s2 && e1 >= e2) || (s1 >= s2 && e1 <= e2)
                })
                .inspect(|_capt| {
                    println!("{line}");
                })
        })
        .collect::<Vec<_>>()
        .len();

    println!("Tot: {tot}");
    println!("The end!");
}

use std::collections::HashMap;
use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

#[derive(Eq, Hash, PartialEq, Clone, Copy)]
enum Shape {
    Rock,
    Paper,
    Scissors,
}

const WIN: u32 = 6;
const DRAW: u32 = 3;
const LOSE: u32 = 0;

fn score(me: Shape) -> u32 {
    match me {
        Shape::Rock => 1,
        Shape::Paper => 2,
        Shape::Scissors => 3,
    }
}

fn decide(them: Shape, goal: u32) -> Shape {
    let mut wins: HashMap<Shape, Shape> = HashMap::new();
    wins.insert(Shape::Paper, Shape::Rock);
    wins.insert(Shape::Rock, Shape::Scissors);
    wins.insert(Shape::Scissors, Shape::Paper);

    let mut loses: HashMap<Shape, Shape> = HashMap::new();
    loses.insert(Shape::Rock, Shape::Paper);
    loses.insert(Shape::Scissors, Shape::Rock);
    loses.insert(Shape::Paper, Shape::Scissors);
    if goal == DRAW {
        them
    } else if goal == WIN {
        loses[&them] // to win, need to find the losing hand for them
    } else {
        wins[&them] // to lose, need to find the winning hand for them
    }
}

fn find_winner(line: String) -> u32 {
    let mut shapes: Vec<&str> = line.split(' ').collect();
    let first = shapes.remove(0);
    let them: Shape;
    if first == "A" {
        them = Shape::Rock;
    } else if first == "B" {
        them = Shape::Paper;
    } else {
        them = Shape::Scissors;
    }
    let second = shapes.remove(0);
    if second == "X" {
        // need to lose
        score(decide(them, LOSE)) + LOSE
    } else if second == "Y" {
        // need to draw
        score(decide(them, DRAW)) + DRAW
    } else {
        // need to win
        score(decide(them, WIN)) + WIN
    }
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2022")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: u32 = 0;
    for line_ in reader.lines() {
        sum += find_winner(line_.unwrap());
    }
    println!("Sum of all of the calibration values: {}", sum);
}

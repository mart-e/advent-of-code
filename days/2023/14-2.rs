use clap::{App, Arg};
use itertools::rev;
use std::{collections::HashMap, fs, ops::Index};

fn extract_columns(lines: String) -> Vec<String> {
    lines
        .lines()
        .map(|l| l.to_string())
        .collect::<Vec<String>>()
}

fn move_rocks(cols: &Vec<String>, dir: usize) -> Vec<String> {
    let mut res = vec![];

    if dir == 1 {
        // NORTH
        let mut res1 = vec![];
        let size = cols.first().expect("empty lines").len();
        for idx in 0..size {
            res1.push(
                cols.iter()
                    .map(|col| col.chars().nth(idx).unwrap())
                    .collect::<String>()
                    .split('#')
                    .map(|part| {
                        "O".repeat(part.chars().filter(|&c| c == 'O').count())
                            + &".".repeat(part.chars().filter(|&c| c != 'O').count())
                    })
                    .collect::<Vec<String>>()
                    .join("#"),
            );
        }

        for idx in 0..cols.len() {
            res.push(
                res1.iter()
                    .map(|l| l.chars().nth(idx).unwrap())
                    .collect::<String>(),
            );
        }
    } else if dir == 2 {
        // WEST
        res = cols
            .iter()
            .map(|col| {
                col.split('#')
                    .map(|part| {
                        "O".repeat(part.chars().filter(|&c| c == 'O').count())
                            + &".".repeat(part.chars().filter(|&c| c != 'O').count())
                    })
                    .collect::<Vec<String>>()
                    .join("#")
            })
            .collect::<Vec<String>>();
    } else if dir == 3 {
        // SOUTH
        let mut res1 = vec![];
        let size = cols.first().expect("empty lines").len();
        for idx in 0..size {
            res1.push(
                cols.iter()
                    .map(|col| col.chars().nth(idx).unwrap())
                    .collect::<String>()
                    .split('#')
                    .map(|part| {
                        ".".repeat(part.chars().filter(|&c| c != 'O').count())
                            + &"O".repeat(part.chars().filter(|&c| c == 'O').count())
                    })
                    .collect::<Vec<String>>()
                    .join("#"),
            );
        }

        for idx in 0..cols.len() {
            res.push(
                res1.iter()
                    .map(|l| l.chars().nth(idx).unwrap())
                    .collect::<String>(),
            );
        }
    } else {
        // EAST
        res = cols
            .iter()
            .map(|col| {
                col.split('#')
                    .map(|part| {
                        ".".repeat(part.chars().filter(|&c| c != 'O').count())
                            + &"O".repeat(part.chars().filter(|&c| c == 'O').count())
                    })
                    .collect::<Vec<String>>()
                    .join("#")
            })
            .collect::<Vec<String>>();
    }
    res
}

fn print_map(cols: &Vec<String>) {
    println!("**********");
    for (idx, col) in cols.iter().enumerate() {
        println!("{} < {}", col, cols.len() - idx);
    }
    println!("**********");
}

fn weight(cols: &Vec<String>) -> usize {
    rev(cols.iter())
        .enumerate()
        .map(|(i, l)| l.chars().filter(|&c| c == 'O').count() * (i + 1))
        .collect::<Vec<usize>>()
        .iter()
        .sum()
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut cols = extract_columns(lines);
    let mut cache: HashMap<String, String> = HashMap::new();
    const ROUNDS: usize = 1_000_000_000;

    let mut visited: Vec<String> = vec![];

    print_map(&cols);
    for _ in 1..=ROUNDS {
        let key = cols.clone().join("*");
        if let Some(offset) = visited.clone().into_iter().position(|v| v == key) {
            let cycle = visited.len() - offset;
            let needed = (ROUNDS - offset) / cycle;
            let remaining = (ROUNDS - offset) % cycle;
            println!(
                "Will reach {} after {} cycles of {} elements + {} rounds",
                ROUNDS, needed, cycle, remaining
            );
            let expected = visited.index(offset + remaining);
            cols = expected
                .split("*")
                .map(|p| p.to_string())
                .collect::<Vec<String>>();
            break;
        }
        cols = move_rocks(&cols, 1);
        cols = move_rocks(&cols, 2);
        cols = move_rocks(&cols, 3);
        cols = move_rocks(&cols, 4);
        visited.push(key.clone());
        cache.insert(key.clone(), cols.join("*").to_string());
    }
    //print_map(&cols);

    let tot = weight(&cols);
    println!("Tot: {tot}\n");

    if filename.contains("input") {
        assert_eq!(tot, 97241);
    } else if filename.contains("example") {
        assert_eq!(tot, 64);
    }

    println!("The end!");
}

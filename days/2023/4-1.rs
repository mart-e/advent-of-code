use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};
use regex::Regex;

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_lines(lines: &str) -> Vec<(&str, &str, &str)> {
    let Ok(re) = Regex::new(r"Card(?: *)(?<id>\d*): (?<winning>[\d ]*) \| (?<mine>[\d ]*)") else {
        panic!("Bad Regex!")
    };
    let mut result: Vec<(&str, &str, &str)> = vec![];
    println!("{lines}");
    for (_, [id, winning, mine]) in re.captures_iter(lines).map(|c| c.extract()) {
        result.push((id, winning, mine));
    }
    result
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };
    // let reader = get_file(filename);
    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut total = 0;

    for (id, winning, mine) in parse_lines(&lines) {
        let wins: Vec<&str> = winning.split(' ').filter(|w| !w.is_empty()).collect();
        // let mines: Vec<&str> = mine.split(' ').collect();
        let mut points: u32 = 1;
        for m in mine.split(' ') {
            if m.is_empty() {
                continue;
            }
            if wins.contains(&m.trim()) {
                points *= 2;
            }
        }
        if points > 1 {
            total += points / 2;
            println!("Game {}: {} points (total: {})", id, points / 2, total);
        }
    }
    println!("Total points: {}", total);
}

use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

const NUMS: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

struct Coordinate {
    value: u32,
    position: usize,
    found: bool,
}

fn find_match(line: &str) -> u32 {
    let mut first: Coordinate = Coordinate {
        value: 0,
        position: 0,
        found: false,
    };
    let mut last: Coordinate = Coordinate {
        value: 0,
        position: 0,
        found: false,
    };

    // first time iterates for written values
    // using this instead of regex to be sure to correctly catch "twone"
    for (idx, num) in NUMS.iter().enumerate() {
        for (pos, _) in line.match_indices(num) {
            if (!first.found) | (pos < first.position) {
                first.position = pos;
                first.value = (idx + 1) as u32;
                first.found = true;
            }
            if (!last.found) | (pos > last.position) {
                last.position = pos;
                last.value = (idx + 1) as u32;
                last.found = true;
            }
        }
    }

    // second time, for each digit
    for (pos, num) in line.chars().enumerate() {
        if num.is_ascii_digit() {
            if (!first.found) | (pos < first.position) {
                first.position = pos;
                first.value = num.to_digit(10).unwrap();
                first.found = true;
            }
            if (!last.found) | (pos > last.position) {
                last.position = pos;
                last.value = num.to_digit(10).unwrap();
                last.found = true;
            }
        }
    }
    println!("{} +{}", line, first.value * 10 + last.value);

    first.value * 10 + last.value
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: u32 = 0;
    for line_ in reader.lines() {
        sum += find_match(&line_.unwrap());
    }
    println!("Sum of all of the calibration values: {}", sum);
}

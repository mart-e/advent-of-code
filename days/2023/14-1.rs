use clap::{App, Arg};
use itertools::rev;
use std::fs;

fn extract_columns(lines: String) -> Vec<String> {
    let size = lines.lines().next().expect("empty lines").len();
    let mut res: Vec<String> = vec!["".to_string(); size];
    for line in lines.lines() {
        for (i, c) in line.chars().enumerate() {
            res[i].push(c);
        }
    }
    res
}

fn move_north(col: &String) -> String {
    let mut res = col
        .split('#')
        .map(|part| {
            "O".repeat(part.chars().filter(|&c| c == 'O').count())
                + &".".repeat(part.chars().filter(|&c| c != 'O').count())
        })
        .collect::<Vec<String>>()
        .join("#");

    // trailing # just to be sure
    res += &"#".repeat(col.len() - res.len());

    res
}

fn calc_weight(col: &String) -> usize {
    rev(col.chars())
        .enumerate()
        .map(|(i, c)| if c == 'O' { i + 1 } else { 0 })
        .collect::<Vec<usize>>()
        .iter()
        .sum()
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut tot = 0;
    for (idx, col) in extract_columns(lines).iter().enumerate() {
        println!("--- {col}");
        let new = move_north(col);
        println!(">>> {new}");
        let weight = calc_weight(&new);
        println!("Column {idx}, total weight: {weight} ({tot})\n");
        tot += weight;
    }

    println!("Tot: {tot}");

    if filename.contains("input") {
        assert_eq!(tot, 103333);
    } else if filename.contains("example") {
        assert_eq!(tot, 136);
    }

    println!("The end!");
}

use clap::{App, Arg};
use core::fmt;

use std::collections::HashMap;
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug)]
struct Rule {
    left: char,
    op: char,
    right: usize,
    target: String,
}
impl Rule {
    fn valid(&self, part: &Part) -> String {
        if self.left == '*' {
            return self.target.clone();
        }
        let val = match self.left {
            'x' => part.x,
            'm' => part.m,
            'a' => part.a,
            's' => part.s,
            _ => panic!("bad left {}", self.left),
        };
        if ((self.op == '>') & (val > self.right)) | ((self.op == '<') & (val < self.right)) {
            return self.target.clone();
        }
        // rule does not apply
        "?".to_string()
    }
}
impl Display for Rule {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.left == '*' {
            write!(f, "-> {}", self.target)
        } else {
            write!(
                f,
                "{} {} {} -> {}",
                self.left, self.op, self.right, self.target
            )
        }
    }
}

#[derive(Debug)]
struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}
impl Part {
    fn value(&self) -> usize {
        self.x + self.m + self.a + self.s
    }

    fn set(&mut self, key: char, val: usize) {
        match key {
            'x' => self.x = val,
            'm' => self.m = val,
            'a' => self.a = val,
            's' => self.s = val,
            _ => panic!("bad key {key}"),
        }
    }
}

fn parse_workflows(lines: String) -> (HashMap<String, Vec<Rule>>, Vec<Part>) {
    let mut rules: HashMap<String, Vec<Rule>> = HashMap::new();
    let mut parts: Vec<Part> = vec![];

    let Some((left, right)) = lines.split_once("\n\n") else {
        panic!("Bad lines")
    };
    for rline in left.lines() {
        let Some((name, mut rules_str)) = rline.split_once('{') else {
            panic!("Not a rule line {rline}")
        };
        rules_str = &rules_str[..rules_str.len() - 1];
        let mut workflow = vec![];
        for rule_str in rules_str.split(',') {
            if let Some((rule, target)) = rule_str.split_once(':') {
                workflow.push(Rule {
                    left: rule.chars().next().unwrap(),
                    op: rule.chars().nth(1).unwrap(),
                    right: rule[2..].parse().unwrap(),
                    target: target.to_string(),
                });
            } else {
                // !rule_str.contains(':')
                workflow.push(Rule {
                    left: '*',
                    op: '=',
                    right: 0,
                    target: rule_str.to_string(),
                });
            }
        }
        rules.insert(name.to_string(), workflow);
    }

    for pline in right.lines() {
        // remove { }
        let pline = &pline[1..pline.len() - 1];
        let mut part = Part {
            x: 0,
            m: 0,
            a: 0,
            s: 0,
        };
        for term in pline.split(',') {
            let Some((c, val)) = term.split_once('=') else {
                panic!("bad workflow term {term}");
            };
            part.set(c.chars().next().unwrap(), val.parse::<usize>().unwrap());
        }
        parts.push(part);
    }

    (rules, parts)
}

fn accepted(part: &Part, rules: &HashMap<String, Vec<Rule>>) -> bool {
    let mut target = "in".to_string();
    while (target != "A") & (target != "R") {
        let Some(workflow) = rules.get(&target) else {
            panic!("No workflow {target}")
        };
        for rule in workflow {
            let res = rule.valid(part);
            if res == "R" {
                return false;
            }
            if res == "A" {
                return true;
            }
            if res == "?" {
                // does not apply, check next rule
                continue;
            }
            // new instruction
            target = res;
            break;
        }
        // end of all rules or broke early
        // if ended in A/R -> return
        // if not, check next target workflow
    }
    panic!("No suitable workflow found");
}

fn main() {
    let (filename, lines) = get_lines();

    let (rules, parts) = parse_workflows(lines);

    let mut tot = 0;
    for part in parts {
        print!("Part {:?}", part);
        if accepted(&part, &rules) {
            println!(" is valid! +{}", part.value());
            tot += part.value();
        } else {
            println!(" is not valid, skip");
        }
    }

    if filename.contains("example") {
        assert_eq!(tot, 19114);
    } else if filename.contains("input") {
        assert_eq!(tot, 373302);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

use clap::{App, Arg};
use core::fmt;
use pathfinding::prelude::dijkstra;
use std::collections::HashMap;
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Coord {
    x: usize,
    y: usize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.x == usize::MAX {
            write!(f, "∞.{}", self.y)
        } else if self.y == usize::MAX {
            write!(f, "{}.∞", self.x)
        } else {
            write!(f, "{}.{}", self.x, self.y)
        }
    }
}
impl Coord {
    // 0 N, 1 E, 2 S, 3 W
    fn walk(&self, dir: usize, map: &GardenMap) -> Option<Coord> {
        let new = match dir {
            0 => {
                if self.y.checked_sub(1).is_some() {
                    Some(Coord {
                        x: self.x,
                        y: self.y - 1,
                    })
                } else {
                    None
                }
            }
            1 => {
                let max_x = map.keys().map(|c| c.x).max().unwrap();
                if self.x < max_x {
                    Some(Coord {
                        x: self.x + 1,
                        y: self.y,
                    })
                } else {
                    None
                }
            }
            2 => {
                let max_y = map.keys().map(|c| c.y).max().unwrap();
                if self.y < max_y {
                    Some(Coord {
                        x: self.x,
                        y: self.y + 1,
                    })
                } else {
                    None
                }
            }
            3 => {
                if self.x.checked_sub(1).is_some() {
                    Some(Coord {
                        x: self.x - 1,
                        y: self.y,
                    })
                } else {
                    None
                }
            }
            _ => panic!("unkown dir"),
        };
        if let Some(newc) = new {
            match *map.get(&newc).unwrap() {
                Tile::Rock => return None,
                _ => return new,
            };
        }
        new
    }
}

#[derive(Clone)]
enum Tile {
    Start,
    Plot,
    Rock,
}

type GardenMap = HashMap<Coord, Tile>;

fn parse_map(lines: String) -> GardenMap {
    let mut map: GardenMap = HashMap::new();
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let v = match c {
                '.' => Tile::Plot,
                'S' => Tile::Start,
                '#' => Tile::Rock,
                _ => panic!("unknown"),
            };
            map.insert(Coord { x, y }, v);
        }
    }
    map
}

fn print_map(map: &GardenMap, pos: &Vec<Coord>) {
    let Some(x_size) = map.keys().map(|c| c.x).max() else {
        panic!("bad map")
    };
    let Some(y_size) = map.keys().map(|c| c.y).max() else {
        panic!("bad map")
    };

    print!("\n");
    for x in 0..=x_size {
        print!("{}", x % 10);
    }
    print!("\n");

    for y in 0..=y_size {
        for x in 0..=x_size {
            let c = Coord { x, y };
            if pos.contains(&c) {
                print!("\x1b[1;34mO\x1b[0;37m");
            } else {
                let Some(t) = map.get(&c) else {
                    panic!("missig {}.{} in map", x, y)
                };
                match t {
                    Tile::Plot => print!("\x1b[1;38m.\x1b[0;37m"),
                    Tile::Rock => print!("\x1b[1;35m#\x1b[0;37m"),
                    Tile::Start => print!("\x1b[1;31mS\x1b[0;37m"),
                };
            }
        }
        println!(" {y}");
    }
    println!("");
}

fn walk(map: &GardenMap, pos: Vec<Coord>) -> Vec<Coord> {
    let mut res = vec![];
    for p in pos {
        for dir in 0..=3 {
            if let Some(new) = p.walk(dir, map) {
                if !res.contains(&new) {
                    res.push(new);
                }
            }
        }
    }
    res
}

fn main() {
    let (filename, lines) = get_lines();

    let map = parse_map(lines);
    let Some(start) = map
        .clone()
        .into_iter()
        .filter_map(|(c, t)| match t {
            Tile::Start => Some(c),
            _ => None,
        })
        .next()
    else {
        panic!("No start found");
    };
    let mut pos = vec![start];
    print_map(&map, &pos);

    let num_steps;
    let expected;
    if filename.contains("example") {
        num_steps = 6;
        expected = 16;
    } else {
        num_steps = 64;
        expected = 3746;
    }

    for step in 1..=num_steps {
        pos = walk(&map, pos);
        println!("At step {}, walking {} positions", step, pos.clone().len());
    }
    print_map(&map, &pos);

    let tot = pos.clone().len();
    println!("Tot {}", tot);

    assert_eq!(tot, expected);

    println!("The end!");
}

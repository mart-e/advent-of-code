use clap::{App, Arg};
use std::fs;
use std::iter::zip;

fn parse_lines(lines: String) -> Vec<(String, Vec<usize>)> {
    let mut res: Vec<(String, Vec<usize>)> = vec![];

    for line in lines.lines() {
        if let Some((left, right)) = line.split_once(' ') {
            res.push((
                left.to_string(),
                right
                    .split(',')
                    .flat_map(|p| p.parse::<usize>())
                    .collect::<Vec<usize>>(),
            ));
        }
    }
    res
}

fn reccur_inspect(s: String) -> Vec<String> {
    //println!("rec: {s}");
    let mut res = vec![];
    let mut news = String::from("");

    for (idx, c) in s.chars().enumerate() {
        if c != '?' {
            news.push(c);
        } else {
            let (_, new) = s.split_at(idx + 1);
            res.append(&mut reccur_inspect(format!("{news}.{new}")));
            res.append(&mut reccur_inspect(format!("{news}#{new}")));
            break; // do not check rest of string
        }
    }
    if news.len() == s.len() {
        // final, no more ?
        res.push(news);
    }
    res
}

fn find_possibles(rec: String, arrangements: Vec<usize>) -> usize {
    println!("find_possibles: {rec} {arrangements:?}");
    let possible_vec = reccur_inspect(rec);
    let arr_len = arrangements.clone().len();

    let mut tot = 0;
    'outer: for pos in possible_vec {
        let groups = pos
            .split('.')
            .filter(|&x| !x.is_empty())
            .collect::<Vec<&str>>();
        if groups.len() != arr_len {
            // impossible
            continue;
        }
        for (found, expected) in zip(groups, arrangements.clone()) {
            if found.len() != expected {
                continue 'outer;
            }
        }
        tot += 1
    }
    tot
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let records = parse_lines(lines);

    let mut tot = 0;
    for (record, arragenements) in records {
        tot += find_possibles(record, arragenements);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

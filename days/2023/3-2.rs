use std::collections::HashMap;
use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_matrix(reader: BufReader<File>) -> Vec<Vec<char>> {
    let mut lines: Vec<Vec<char>> = vec![];
    for line_ in reader.lines() {
        let line = line_.unwrap().clone();
        lines.push(line.chars().collect());
    }
    lines
}

fn find_gear(positions: &Vec<(usize, usize)>, lines: &[Vec<char>]) -> Option<(usize, usize)> {
    for (x, y) in positions.iter() {
        if (*x >= lines.len()) || (*y >= lines[*x].len()) {
            // out of bound
            continue;
        }
        if lines[*x][*y] == '*' {
            // found gear, return coordinates
            return Some((*x, *y));
        }
    }
    // no gear around this part
    None
}

fn find_numbers(lines: Vec<Vec<char>>) -> u32 {
    let mut sum: u32 = 0;
    let mut gears: HashMap<(usize, usize), u32> = HashMap::new();

    // list of positions to inspect
    let mut positions: Vec<(usize, usize)> = vec![];

    for (x, line) in lines.iter().enumerate() {
        let mut part: u32 = 0;
        for (y, c) in line.iter().enumerate() {
            if c.is_ascii_digit() {
                if part == 0 {
                    // start of a new part number
                    positions.clear();
                    if let Some(p) = c.to_digit(10) {
                        part = p
                    }
                    // verify if a symbol is present
                    //  ?..
                    //  ?X.
                    //  ?..
                    if let Some(y1) = y.checked_sub(1) {
                        if let Some(x1) = x.checked_sub(1) {
                            positions.push((x1, y1));
                        }
                        positions.push((x, y1));
                        positions.push((x + 1, y1));
                    };
                } else {
                    if let Some(cd) = c.to_digit(10) {
                        part = 10 * part + cd;
                    }
                }
                //  .?.
                //  .X.
                //  .?.
                if let Some(x1) = x.checked_sub(1) {
                    positions.push((x1, y));
                }
                positions.push((x + 1, y));

                if y == line.len() - 1 {
                    // part number at the end of the file
                    if let Some((xg, yg)) = find_gear(&positions, &lines) {
                        println!("found a gear at {}/{}", xg, yg);
                        if let Some(r) = gears.get(&(xg, yg)) {
                            println!(
                                "... already had a known part {} using this gear! Multiply by {}",
                                r, part
                            );
                            sum += r * part;
                        } else {
                            gears.insert((xg, yg), part);
                        }
                    }
                }
            } else {
                if part > 0 {
                    // finish counting a part number
                    //  ..?
                    //  .X?
                    //  ..?
                    if let Some(x1) = x.checked_sub(1) {
                        positions.push((x1, y));
                    }
                    positions.push((x, y));
                    positions.push((x + 1, y));
                    if let Some((xg, yg)) = find_gear(&positions, &lines) {
                        println!("found a gear at {}/{}", xg, yg);
                        if let Some(r) = gears.get(&(xg, yg)) {
                            println!(
                                "... already had a known part {} using this gear! Multiply by {}",
                                r, part
                            );
                            sum += r * part;
                        } else {
                            gears.insert((xg, yg), part);
                        }
                    }

                    part = 0;
                }
            }
        }
    }
    sum
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    if let Some(filename) = args.value_of("filename") {
        let reader = get_file(filename);

        let lines = parse_matrix(reader);
        let total = find_numbers(lines);

        println!("Total parts: {}", total);
    }
}

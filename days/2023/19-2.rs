use clap::{App, Arg};
use core::fmt;

use std::collections::HashMap;
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

fn parse_workflows(lines: String) -> HashMap<String, Vec<Rule>> {
    let mut rules: HashMap<String, Vec<Rule>> = HashMap::new();

    let Some((left, _)) = lines.split_once("\n\n") else {
        panic!("Bad lines")
    };
    for rline in left.lines() {
        let Some((name, mut rules_str)) = rline.split_once('{') else {
            panic!("Not a rule line {rline}")
        };
        rules_str = &rules_str[..rules_str.len() - 1];
        let mut workflow = vec![];
        for rule_str in rules_str.split(',') {
            if let Some((rule, target)) = rule_str.split_once(':') {
                workflow.push(Rule {
                    left: rule.chars().next().unwrap(),
                    op: rule.chars().nth(1).unwrap(),
                    right: rule[2..].parse().unwrap(),
                    target: target.to_string(),
                });
            } else {
                // !rule_str.contains(':')
                workflow.push(Rule {
                    left: '*',
                    op: '=',
                    right: 0,
                    target: rule_str.to_string(),
                });
            }
        }
        rules.insert(name.to_string(), workflow);
    }
    rules
}

#[derive(Debug)]
struct Rule {
    left: char,
    op: char,
    right: usize,
    target: String,
}
impl Rule {
    fn check(&self, part: &PartR) -> Vec<(PartR, String)> {
        let FALLBACK: String = "?".to_string();

        if self.left == '*' {
            // end of workflow, fallback on target
            return vec![(*part, self.target.clone())];
        }
        let (min, max) = match self.left {
            'x' => (part.x_min, part.x_max),
            'm' => (part.m_min, part.m_max),
            'a' => (part.a_min, part.a_max),
            's' => (part.s_min, part.s_max),
            _ => panic!("bad left {}", self.left),
        };
        if self.op == '>' {
            if min > self.right {
                // [100..200] x > 10 -> [([100..200], target)]
                println!(
                    "{}: {} > {} -> move {} to {}",
                    self.left, min, self.right, part, self.target
                );
                vec![(*part, self.target.clone())]
            } else if max > self.right {
                // [0..200] x > 100 -> [([0..100], FALLBack), ([101..200], target)]
                let (left, right) = part.split(self.left, self.right);
                println!(
                    "{}: {} > {} -> split into {} - {}",
                    self.left, max, self.right, left, right
                );
                vec![(left, FALLBACK), (right, self.target.clone())]
            } else {
                // [0..100] x > 200 -> [([0..200], FALLBACK)]
                println!("{} > {} -> not apply to {}", self.left, self.right, part);
                vec![(*part, FALLBACK)]
            }
        } else {
            if max < self.right {
                // [0..100] x < 200 -> [([0..200], target)]
                println!(
                    "{}: {} < {} -> move {} to {}",
                    self.left, max, self.right, part, self.target
                );
                vec![(*part, self.target.clone())]
            } else if min < self.right {
                // [0..200] x < 100 -> [([0..99], target), ([100..200], FALLBACK)]
                let (left, right) = part.split(self.left, self.right - 1);
                println!(
                    "{}: {} < {} -> split into {} - {}",
                    self.left, min, self.right, left, right
                );
                vec![(left, self.target.clone()), (right, FALLBACK)]
            } else {
                // [100..200] x < 10 -> [([100..200], FALLBACK)]
                println!("{} < {} -> not apply to {}", self.left, self.right, part);
                vec![(*part, FALLBACK)]
            }
        }
    }
}
impl Display for Rule {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.left == '*' {
            write!(f, "-> {}", self.target)
        } else {
            write!(
                f,
                "{} {} {} -> {}",
                self.left, self.op, self.right, self.target
            )
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct PartR {
    x_min: usize,
    x_max: usize,
    m_min: usize,
    m_max: usize,
    a_min: usize,
    a_max: usize,
    s_min: usize,
    s_max: usize,
}
impl PartR {
    fn value(&self) -> usize {
        (self.x_max - self.x_min + 1)
            * (self.m_max - self.m_min + 1)
            * (self.a_max - self.a_min + 1)
            * (self.s_max - self.s_min + 1)
    }

    // [0..200].split(x, 100) -> ([0..100], [101..200])
    fn split(&self, key: char, val: usize) -> (PartR, PartR) {
        match key {
            'x' => (
                PartR {
                    x_max: val,
                    ..*self
                },
                PartR {
                    x_min: val + 1,
                    ..*self
                },
            ),
            'm' => (
                PartR {
                    m_max: val,
                    ..*self
                },
                PartR {
                    m_min: val + 1,
                    ..*self
                },
            ),
            'a' => (
                PartR {
                    a_max: val,
                    ..*self
                },
                PartR {
                    a_min: val + 1,
                    ..*self
                },
            ),
            's' => (
                PartR {
                    s_max: val,
                    ..*self
                },
                PartR {
                    s_min: val + 1,
                    ..*self
                },
            ),
            _ => panic!("bad key {key}"),
        }
    }

    fn refine(&self, rules: &HashMap<String, Vec<Rule>>, start: String) -> usize {
        println!("Refine for {} at step {}", self, start);
        let mut tot = 0;
        let mut to_check = vec![(*self, start)];

        // to_check is the list of (part, target) to process
        while !to_check.is_empty() {
            let mut new = vec![];
            for (mut part, target) in to_check {
                // one target => one workflow
                let Some(workflow) = rules.get(&target) else {
                    panic!("No workflow {target}")
                };

                // each workflow has a set of rules
                for rule in workflow {
                    // for each rule checked, can result in one or two parts
                    // (one has another target the other check on the other rules)
                    for (new_part, new_target) in rule.check(&part) {
                        if new_target == "R" {
                            // dead end, range value 0
                            continue;
                        }
                        if new_target == "A" {
                            // range accepted
                            println!("Range {} accepted! +{}", new_part, new_part.value());
                            tot += new_part.value();
                            continue;
                        }
                        if new_target == "?" {
                            // rule does not apply
                            part = new_part;
                            continue;
                        } else {
                            // new target to check in the next round
                            new.push((new_part, new_target.clone()));
                        }
                    }
                }
            }
            to_check = new;
        }
        tot
    }
}

impl Display for PartR {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "x:{}..{} m:{}..{}, a:{}..{}, s:{}..{}",
            self.x_min,
            self.x_max,
            self.m_min,
            self.m_max,
            self.a_min,
            self.a_max,
            self.s_min,
            self.s_max,
        )
    }
}

fn main() {
    let (filename, lines) = get_lines();

    let rules = parse_workflows(lines);

    let part = PartR {
        x_min: 1,
        x_max: 4000,
        m_min: 1,
        m_max: 4000,
        a_min: 1,
        a_max: 4000,
        s_min: 1,
        s_max: 4000,
    };
    let tot = part.refine(&rules, "in".to_string());

    if filename.contains("example") {
        assert_eq!(tot, 167409079868000);
    } else if filename.contains("input") {
        assert_eq!(tot, 130262715574114);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

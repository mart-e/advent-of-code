use clap::{App, Arg};
use std::{fs, collections::HashMap};

fn parse_lines(lines: String, repeat: usize) -> Vec<(String, Vec<usize>)> {
    lines
        .lines()
        .map(|line| {
            (
                // left part join with "?" repeated - last char
                ((line.split_once(' ').unwrap().0.to_string() + "?").repeat(repeat))
                    .strip_suffix(|_: char| true)
                    .unwrap()
                    .to_string(),
                // right part, split by , parsed to int repeated
                line.split_once(' ')
                    .unwrap()
                    .1
                    .split(',')
                    .flat_map(|p| p.parse::<usize>())
                    .collect::<Vec<usize>>()
                    .repeat(repeat),
            )
        })
        .collect()
}

fn count_possibles(line: String, arr: Vec<usize>, cache: &mut HashMap<(String, Vec<usize>), usize>) -> usize {
    if line.is_empty() {
        if arr.is_empty() {
	    // victory !
	    return 1;
	} else {
	    // lenght diff
	    return 0;
	}
    }
    if line.starts_with('.') {
	// ignore, check next string
	let key = (line.split_at(1).1.to_string(), arr);
	// block possible, check substring
	if let Some(&entry) = cache.get(&key) {
	    return entry;
	} else {
	    let count = count_possibles(key.clone().0, key.clone().1, cache);
	    cache.insert(key, count);
	    return count;
	}
	// return count_possibles(line.split_at(1).1.to_string(), arr, cache);
    }
    if line.starts_with('#') {
        if let Some(&size) = arr.first() {
	    // not enough char remaining
	    if (size > line.len()) ||
		// block too long
		(line.chars().nth(size) == Some('#')) || 
		// not a full block
		(line[..size].contains('.'))
            {
                return 0;
            }
	    let key = (line.split_at((size+1).min(line.len())).1.to_string(), arr[1..].to_vec());
	    // block possible, check substring
	    if let Some(&entry) = cache.get(&key) {
		entry
	    } else {
		let count = count_possibles(key.clone().0, key.clone().1, cache);
		cache.insert(key, count);
		count
	    }
        } else {
	    // arr is empty
            0
        }
    } else {
	// is a ?, check both
	let key1 = (format!(".{}", line.split_at(1).1), arr.clone());
	let mut res = 0;
	if let Some(&entry) = cache.get(&key1) {
	    res += entry;
	} else {
	    let count = count_possibles(key1.clone().0, key1.clone().1, cache);
	    cache.insert(key1, count);
	    res += count;
	}

	let key2 = (format!("#{}", line.split_at(1).1), arr.clone());
	if let Some(&entry) = cache.get(&key2) {
	    res += entry;
	} else {
	    let count = count_possibles(key2.clone().0, key2.clone().1, cache);
	    cache.insert(key2, count);
	    res += count;
	}
	res
    }
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut tot = 0;
    for (count, (s, a)) in parse_lines(lines, 5).into_iter().enumerate() {
	let mut cache: HashMap<(String, Vec<usize>), usize> = HashMap::new();
        print!("#{}: {} {:?}", count+1, s, a);
        let res = count_possibles(s, a, &mut cache);
	println!("... + {res} (tot {tot}) {}", cache.len());
	tot += res;
    }

    println!("Tot: {tot}");
    println!("The end!");
}

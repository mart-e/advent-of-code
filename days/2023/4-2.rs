use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};
use regex::Regex;

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_lines(lines: &str) -> Vec<(&str, &str, &str)> {
    let Ok(re) = Regex::new(r"Card(?: *)(?<id>\d*): (?<winning>[\d ]*) \| (?<mine>[\d ]*)") else {
        panic!("Bad Regex!")
    };
    let mut result: Vec<(&str, &str, &str)> = vec![];
    println!("{lines}");
    for (_, [id, winning, mine]) in re.captures_iter(lines).map(|c| c.extract()) {
        result.push((id, winning, mine));
    }
    result
}

fn increment_stack(stack: &mut Vec<u32>, num: u32, start: u32) {
    let multiply: u32 = stack[start as usize];
    println!("Stack was {:?}", stack);
    for x in (start + 1)..(start + num + 1) {
        match stack.get(x as usize) {
            Some(occ) => stack[x as usize] = occ + multiply,
            None => panic!("trying to access out of bound item {start}"),
        }
    }
    println!("Stack is now {:?}", stack);
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut stack: Vec<u32> = vec![];
    // TODO properly initialize
    for _ in lines.lines() {
        // has one card of earch line
        stack.push(1);
    }
    for (id, winning, mine) in parse_lines(&lines) {
        let wins: Vec<&str> = winning.split(' ').filter(|w| !w.is_empty()).collect();

        let mut cards: u32 = 0;
        for m in mine.split(' ') {
            if m.is_empty() {
                continue;
            }
            if wins.contains(&m.trim()) {
                cards += 1;
            }
        }
        println!("end of line {id}, total cards {cards}");
        if cards > 0 {
            let Ok(start) = id.parse::<u32>() else {
                panic!("Bad line id {id}")
            };
            increment_stack(&mut stack, cards, start - 1);
        }
    }
    let total: u32 = stack.iter().sum();
    println!("Total points: {}", total);
}

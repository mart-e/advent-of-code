use clap::{App, Arg};
use core::fmt;

use std::collections::{HashMap, VecDeque};
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

// stolen from https://github.com/cainkellye/advent_of_code/blob/main/src/y2023/day20.rs
fn name_to_id(name: String) -> usize {
    if name == "broadcaster" {
        return 0;
    }
    let mut id = 0;
    for (exp, byte) in name.bytes().enumerate() {
        id += (byte as usize) * 256_usize.pow(exp as u32);
    }
    //println!("name_to_id {} = {}", name, id);
    id
}

fn parse_destinations(lines: String) -> (ModuleMap, LinksMap) {
    let mut map: ModuleMap = HashMap::new();
    let mut connections: LinksMap = HashMap::new();

    // first time, parse list of buttons
    for line in lines.clone().lines() {
        let (left, _) = line.split_once(" -> ").unwrap();
        match left.chars().next().unwrap() {
            '%' => {
                let name = left[1..].to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(FlipFlop {
                        //id: name_to_id(name.clone()),
                        name,
                        last: false,
                    }),
                );
            }
            '&' => {
                let name = left[1..].to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(Conjonction {
                        //id: name_to_id(name.clone()),
                        name,
                        inputs: HashMap::new(),
                    }),
                );
            }
            _ => {
                let name = left.to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(Broadcast {
                        //id: name_to_id(name.clone()),
                        name,
                    }),
                );
            }
        };
    }

    // second time, make connections
    for line in lines.clone().lines() {
        let (left, right) = line.split_once(" -> ").unwrap();
        let src = match left.chars().next().unwrap() {
            '%' | '&' => name_to_id(left[1..].to_string()),
            _ => name_to_id(left.to_string()),
        };

        let v: Vec<usize> = right
            .split(',')
            .map(|n| name_to_id(n.trim().to_string()))
            .collect();
        connections.insert(src, v);
    }

    // find input connections for each module
    for (mid, module) in &mut map {
        module.connect(
            connections
                .clone()
                .into_iter()
                .filter(|(_, dst)| dst.contains(&mid))
                .map(|(src, _)| src)
                .collect(),
        );
    }

    (map, connections)
}

// to retrieve a Module from his number
type ModuleMap = HashMap<usize, Box<dyn Module>>;
// to retrieve the incoming/outgoing links from a module number
type LinksMap = HashMap<usize, Vec<usize>>;

trait Module {
    fn receive(&mut self, from: usize, high: bool) -> Option<bool>;
    fn connect(&mut self, inputs: Vec<usize>);
    fn name(&self) -> String;
}

struct Broadcast {
    //id: usize,
    name: String,
}
impl Module for Broadcast {
    fn receive(&mut self, _: usize, high: bool) -> Option<bool> {
        Some(high)
    }
    fn connect(&mut self, _: Vec<usize>) {}
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct FlipFlop {
    //id: usize,
    name: String,
    last: bool,
}
impl Module for FlipFlop {
    fn receive(&mut self, _: usize, high: bool) -> Option<bool> {
        if high {
            None
        } else {
            self.last = !self.last;
            Some(self.last)
        }
    }
    fn connect(&mut self, _: Vec<usize>) {}
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct Conjonction {
    //id: usize,
    name: String,
    inputs: HashMap<usize, bool>,
}
impl Module for Conjonction {
    fn receive(&mut self, from: usize, high: bool) -> Option<bool> {
        // update last pulse
        self.inputs.entry(from).and_modify(|v| *v = high);
        if self.inputs.iter().all(|(_, &inp)| inp) {
            // all inputs are high -> return low
            Some(false)
        } else {
            Some(true)
        }
    }
    fn connect(&mut self, inputs: Vec<usize>) {
        inputs.into_iter().for_each(|input| {
            self.inputs.insert(input, false);
        })
    }
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct Message {
    src: usize,
    dst: usize,
    high: bool,
}

impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.high {
            write!(f, "{} -high-> {}", self.src, self.dst)
        } else {
            write!(f, "{} -low-> {}", self.src, self.dst)
        }
    }
}

fn send_pulse(
    message: Message,
    connections: &LinksMap,
    map: &mut ModuleMap,
    targets: &Vec<usize>,
) -> Vec<Message> {
    if targets.contains(&message.dst) && !message.high {
        // found one of the target
        return vec![Message {
            src: message.dst,
            dst: message.dst,
            high: true,
        }];
    }
    let Some(module) = map.get_mut(&message.dst) else {
        //panic!("No module {} found", message.dst);
        return vec![];
    };
    if let Some(pulse) = module.receive(message.src, message.high) {
        let Some(r) = connections.get(&message.dst) else {
            panic!("No connection found for {}", message.dst);
        };
        return r
            .iter()
            .map(|&n| Message {
                src: message.dst,
                dst: n,
                high: pulse,
            })
            .collect();
    }
    vec![]
}

fn main() {
    let (filename, lines) = get_lines();

    let (mut map, connections) = parse_destinations(lines);
    let mut low_count = 0;
    let mut high_count = 0;
    let mut press = 0;

    let goal: usize = name_to_id("rx".to_string());
    let before: usize = connections
        .clone()
        .into_iter()
        .filter(|(_, dst)| dst.contains(&goal))
        .map(|(src, _)| src)
        .next()
        .unwrap();
    let targets: Vec<usize> = connections
        .clone()
        .into_iter()
        .filter(|(_, dst)| dst.contains(&before))
        .map(|(src, _)| src)
        .collect();

    println!("Goal {goal}");
    println!("Before {before}");
    println!("Targets {targets:?}");

    let mut target_map: HashMap<usize, usize> =
        targets.clone().into_iter().map(|id| (id, 0)).collect();
    'outer: loop {
        press += 1;
        let mut messages: VecDeque<Message> = VecDeque::from([Message {
            src: usize::MAX,
            dst: name_to_id("broadcaster".to_string()),
            high: false,
        }]);
        while !messages.is_empty() {
            let me = messages.pop_front().expect("empty messages");
            if me.src == me.dst {
                target_map.entry(me.src).and_modify(|v| *v = press);
                println!(
                    "Got low on {} after {} pulse {:?}",
                    me.src, press, target_map
                );
                if target_map.clone().into_iter().all(|(_, v)| v != 0) {
                    break 'outer;
                }
            } else {
                if me.high {
                    high_count += 1;
                } else {
                    low_count += 1;
                }
                send_pulse(me, &connections, &mut map, &targets)
                    .into_iter()
                    .for_each(|m| messages.push_back(m));
            }
        }
    }
    println!(
        "Now go compute the LCM of {:?}",
        target_map
            .clone()
            .into_iter()
            .map(|(_, v)| v)
            .collect::<Vec<usize>>()
    );

    println!("The end!");
}

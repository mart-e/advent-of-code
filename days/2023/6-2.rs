use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_lines(lines: String) -> (u64, u64) {
    let mut all: Vec<u64> = vec![];
    for line in lines.lines() {
        let mut vals: u64 = 0;
        for c in line.chars() {
            if c.is_ascii_digit() {
                vals *= 10;
                match c.to_digit(10) {
                    Some(d) => vals += d as u64,
                    _ => panic!("can't convert {c}"),
                }
            }
        }
        all.push(vals);
    }
    let Some(time) = all.first() else {
        panic!("time line not found")
    };
    let Some(distance) = all.iter().nth(1) else {
        panic!("distance line not found")
    };
    (*time, *distance)
}

fn find_winning(time: u64, dist: u64) -> u32 {
    let mut wins = 0;
    let mut already_won = false;
    for t in 0..=time {
        let remain = time - t;
        let possible: u64 = remain * t;
        if possible > dist {
            // println!("push {}, travel {} (goal {})", t, remain * t, dist);
            wins += 1;
            already_won = true;
        } else if already_won {
            // has won in the past, not anymore, no need to continue
            println!("break early at {t}");
            break;
        }
    }
    wins
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    let goal: (u64, u64) = parse_lines(lines);
    println!("{:?}", goal);

    let wins = find_winning(goal.0, goal.1);
    println!("ending win {wins}");
}

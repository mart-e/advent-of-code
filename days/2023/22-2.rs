use clap::{App, Arg};
use core::fmt;
use itertools::Itertools;
use regex::{Captures, Regex};
use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt::{Debug, Display};
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone, PartialOrd, Ord)]
struct Coord {
    x: usize,
    y: usize,
    z: usize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{},{},{}", self.x, self.y, self.z)
    }
}

#[derive(Clone)]
struct Brick {
    start: Coord,
    end: Coord, // input property : start <= end
}
impl Brick {
    fn new(capt: Captures) -> Brick {
        Brick {
            start: Coord {
                x: capt["x1"].parse().unwrap(),
                y: capt["y1"].parse().unwrap(),
                z: capt["z1"].parse().unwrap(),
            },
            end: Coord {
                x: capt["x2"].parse().unwrap(),
                y: capt["y2"].parse().unwrap(),
                z: capt["z2"].parse().unwrap(),
            },
        }
    }

    fn intersect(&self, other: &Brick) -> bool {
        // https://silentmatt.com/rectangle-intersection/
        self.start.x <= other.end.x
            && self.end.x >= other.start.x
            && self.start.y <= other.end.y
            && self.end.y >= other.start.y
    }
    // is self on top of other?
    // e.g. 2,5,8~2,6,8 vs 1,4,6~3,6,7
    #[allow(dead_code)]
    fn on_top(&self, other: &Brick) -> bool {
        (other.end.z <= self.start.z) && (self.start.z - other.end.z == 1) && self.intersect(other)
    }

    fn fall(&mut self, maxh: usize) {
        let diff = self.start.z - maxh - 1;
        self.start.z -= diff;
        self.end.z -= diff;
    }
}
impl Display for Brick {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}~{}", self.start, self.end)
    }
}
impl Debug for Brick {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}~{}", self.start, self.end)
    }
}

fn parse_bricks(lines: String) -> Vec<Brick> {
    let Ok(re) = Regex::new(r"(?<x1>\d*),(?<y1>\d*),(?<z1>\d*)~(?<x2>\d*),(?<y2>\d*),(?<z2>\d*)")
    else {
        panic!("bad regex");
    };
    lines
        .lines()
        .filter_map(|line| re.captures(line).map(|capt| Brick::new(capt)))
        .collect_vec()
}

#[allow(dead_code)]
fn print_bricks(map: &HashMap<usize, Brick>) {
    for (brick_id, brick) in map
        .clone()
        .into_iter()
        .sorted_by_key(|(_, b)| (b.end.z, b.end.x))
    {
        println!("{brick_id} {brick:?}");
    }
}

fn main() {
    let (_, lines) = get_lines();

    let mut brick_map = HashMap::new();
    let mut brick_keys = vec![];
    parse_bricks(lines)
        .into_iter()
        // sorting so check gravity first on the lowest
        .sorted_by_key(|b| b.end.z)
        .enumerate()
        .for_each(|(i, b)| {
            brick_keys.push(i);
            brick_map.insert(i, b);
        });

    let mut support_map: HashMap<usize, HashSet<usize>> = HashMap::new();
    let mut supported_map: HashMap<usize, HashSet<usize>> = HashMap::new();

    // gravity, let bricks fall on each other
    for brick_id in brick_keys.clone().into_iter() {
        let mut brick = brick_map.get(&brick_id).unwrap().clone();

        // all the bricks on which it can fall
        let highest_below = brick_map
            .clone()
            .into_iter()
            .filter(|(y, brick2)| {
                brick_id != *y && brick.start.z > brick2.end.z && brick.intersect(brick2)
            })
            .max_set_by(|(_, a), (_, b)| a.end.z.cmp(&b.end.z));

        if !highest_below.is_empty() {
            highest_below.into_iter().for_each(|(below_id, below)| {
                // won't fall more than once
                brick.fall(below.end.z);
                support_map.entry(below_id).or_default().insert(brick_id);
                supported_map.entry(brick_id).or_default().insert(below_id);
            });
        } else {
            // fall to the ground
            brick.fall(0)
        };
        brick_map.insert(brick_id, brick);
    }

    // nodes supporting nothing can safely be desintegrated
    let mut disintegrated: HashSet<usize> = brick_keys
        .clone()
        .into_iter()
        .filter(|k| !support_map.contains_key(k))
        .collect();
    let mut tot = disintegrated.len();
    println!("Can safely disintigrate {tot} alone bricks");

    support_map
        .clone()
        .into_iter()
        .filter(|(_, tops)| {
            // the brick supports other bricks but
            // can be disintegrated if all those are supported by at least another
            tops.iter()
                .filter(|&top| supported_map.get(top).unwrap().len() > 1)
                .count()
                == tops.len()
        })
        .for_each(|(key, _)| {
            disintegrated.insert(key);
        });
    println!(
        "+{} bricks relying on more than one",
        disintegrated.len() - tot
    );

    tot = disintegrated.len();
    println!("Tot part1: {}", tot);

    tot = 0;
    for brick_id in brick_keys {
        let mut falling: HashSet<usize> = HashSet::from([brick_id]);
        let mut to_check = VecDeque::from([brick_id]);
        while !to_check.is_empty() {
            let suspect = to_check.pop_front().unwrap();
            if let Some(aboves) = support_map.get(&suspect) {
                for above in aboves {
                    // will fall if not supported by other than `supports`
                    if supported_map
                        .get(above)
                        .unwrap()
                        .iter()
                        .all(|x| falling.contains(x))
                    {
                        falling.insert(*above);
                        to_check.push_back(*above);
                    }
                }
            } // else = nothing above suspects
        }
        if falling.len() > 1 {
            println!(
                "If I remove {}, {} bricks will fall",
                brick_id,
                falling.len() - 1
            );
            // -1 as brick_id is disintegrated
            tot += falling.len() - 1;
        }
    }

    println!("Tot: {tot}");
    println!("The end!");
}

use std::{fs, str::Lines};

use clap::{App, Arg};

fn to_histories(lines: Lines) -> Vec<Vec<isize>> {
    let mut res: Vec<Vec<isize>> = vec![];
    for line in lines {
        res.push(
            line.split_whitespace()
                .map(|x| x.parse::<isize>().unwrap())
                .collect(),
        );
    }
    println!("{:?}", res);
    res
}

fn find_stable(start: &Vec<isize>) -> isize {
    let mut firsts = vec![];
    let mut line = start.clone();
    'outer: loop {
        let mut new = vec![];
        let mut prev: isize = isize::MAX;
        for &step in &line {
            if prev == isize::MAX {
                prev = step;
                firsts.push(step);
                continue;
            }
            new.push(step - prev);
            prev = step;
        }
        println!("new {:?}", new);

        line = new;
        if line.iter().all(|x| *x == 0) {
            break 'outer;
        }
    }
    println!("progress {:?}", firsts);
    let mut last = 0;
    for &first in firsts.iter().rev() {
        println!("last: {last} -= {first}");
        last = first - last;
    }

    println!("last {last}");
    last
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let histories: Vec<Vec<isize>> = to_histories(lines.lines());
    let tot = histories.iter().map(|h| find_stable(h)); //.to_vec().sum();
    println!("TOT {}", tot.into_iter().sum::<isize>());
    // for history in histories {
    //     find_stable(history);
    // }
    println!("The end!");
}

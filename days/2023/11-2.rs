use clap::{App, Arg};
use itertools::Itertools;
use std::{cmp::Ordering, collections::HashMap, fs, hash::Hash};

#[derive(Eq, Hash, PartialEq, Clone, Debug)]
struct Coor {
    x: usize,
    y: usize,
}
impl Ord for Coor {
    fn cmp(&self, other: &Self) -> Ordering {
        self.y.cmp(&other.y).then(self.x.cmp(&other.y))
    }
}
impl PartialOrd for Coor {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if let Some(res) = self.y.partial_cmp(&other.y) {
            Some(res)
        } else {
            self.x.partial_cmp(&other.x)
        }
    }
}

fn find_galaxies(lines: String) -> Vec<Coor> {
    let mut res: Vec<Coor> = vec![];
    for (y, line) in lines.lines().enumerate() {
        for (x, _) in line.chars().enumerate().filter(|(_, c)| *c == '#') {
            res.push(Coor { x, y });
        }
    }
    // let z = lines
    //     .lines()
    //     .enumerate()
    //     .map(|(y, l)| {
    //         l.chars().enumerate().filter(|(_, c)| *c == '#')
    //         //.map(move |(x, c)| if c == '#' { Coor { x, y } } else { MAXC })
    //     })
    //     .filter(|c| true);
    // for l in z {
    //     println!("{:?}", l);
    // }

    println!("{:?}", res);

    res
}

fn compute_expanded(start: Vec<Coor>) -> Vec<Coor> {
    let bind = start.clone();
    let mut exp_map: HashMap<Coor, Coor> = start
        .iter()
        .map(|c| (c.clone(), c.clone()))
        .collect::<HashMap<Coor, Coor>>();

    let max_y = bind.iter().map(|c| c.y).max().unwrap();
    let max_x = bind.iter().map(|c| c.x).max().unwrap();

    for x in 0..=max_x {
        if !bind.iter().any(|c| c.x == x) {
            println!("column {x} expanded");
            for c in &bind {
                if c.x > x {
                    let c2 = exp_map.entry(c.clone()).or_insert(c.clone());
                    println!("expand {:?} -> {}/{}", c2, c2.x + 1, c2.y);
                    c2.x += 1_000_000 - 1;
                }
            }
        }
    }
    for y in 0..=max_y {
        if !bind.iter().any(|c| c.y == y) {
            println!("line {y} expanded");
            for c in &bind {
                if c.y > y {
                    let c2 = exp_map.entry(c.clone()).or_insert(c.clone());
                    println!("expand {:?} -> {}/{}", c2, c2.x, c2.y + 1);
                    c2.y += 1_000_000 - 1;
                }
            }
        }
    }
    exp_map.values().cloned().sorted().collect()
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let galaxies: Vec<Coor> = find_galaxies(lines);
    let expanded_galaxies: Vec<Coor> = compute_expanded(galaxies);
    println!("{:?}", expanded_galaxies);

    let mut tot = 0;
    for g1 in &expanded_galaxies {
        for g2 in &expanded_galaxies {
            if (g1 == g2) | (g2.y < g1.y) | ((g2.y == g1.y) & (g2.x < g1.x)) {
                continue;
            }
            let dist = g2.x.abs_diff(g1.x) + g2.y.abs_diff(g1.y);
            tot += dist;
            println!(
                "Distance between {:?} and {:?} : {} ({})",
                g1, g2, dist, tot
            );
        }
    }

    println!("Tot: {tot}");
    println!("The end!");
}

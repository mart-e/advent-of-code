use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_seeds(line: &str) -> Vec<SeedRange> {
    let mut seeds = line.split(' ');
    let mut res: Vec<SeedRange> = vec![];
    let _ = seeds.next(); // first "Seed :"
    loop {
        let Some(seed) = seeds.next() else {
            break;
        };
        let Some(seed2) = seeds.next() else {
            panic!("odd number of seeds");
        };
        let Ok(seed_num) = seed.parse::<i64>() else {
            panic!("can't parse seed {seed}");
        };
        let Ok(repeat) = seed2.parse::<i64>() else {
            panic!("can't parse repeat {seed}");
        };
        res.push(SeedRange {
            start: seed_num,
            end: seed_num + repeat,
        });
    }

    res
}

struct Map {
    start: i64,
    end: i64,
    delta: i64,
}

#[derive(Debug)]
struct SeedRange {
    start: i64,
    end: i64,
}

fn find_path_range(range: SeedRange, maps: &Vec<Vec<Map>>) -> i64 {
    let mut ranges: Vec<SeedRange> = vec![range];
    // seed-to-soil, etc
    for vmap in maps {
        let mut new_ranges: Vec<SeedRange> = vec![];
        // 79..93, etc
        for mut r in ranges {
            let mut range_covered = false;
            // 50 98 2, etc.
            for step in vmap {
                println!(
                    "step: {}, {} (+{}); range: {}, {}",
                    step.start, step.end, step.delta, r.start, r.end
                );
                if (step.start > r.end) | (step.end < r.start) {
                    // range 10..20, step 30..40, unchanged
                    // range 10..20, step 0..5, unchanged
                    continue;
                } else if (step.start <= r.start) & (step.end <= r.end) {
                    // range 10..20, step 5..15
                    // => range 10..15 (+delta) ; 16..20
                    println!(
                        "case 1, split in {}..{} (+{}) (and {}..{})",
                        r.start,
                        step.end,
                        step.delta,
                        step.end + 1,
                        r.end
                    );
                    new_ranges.push(SeedRange {
                        start: r.start + step.delta,
                        end: step.end + step.delta,
                    });
                    if step.end < r.end {
                        r.start = step.end + 1; // new start of current range
                    } else {
                        range_covered = true;
                        break;
                    }
                } else if (step.start >= r.start) & (step.end >= r.end) {
                    // range 10..20, step 15..25
                    // => range 10..14 ; 15..20 (+delta)
                    println!(
                        "case 2, split in ({}..{} and) {}..{} (+{})",
                        r.start,
                        step.start - 1,
                        step.start,
                        r.end,
                        step.delta,
                    );
                    new_ranges.push(SeedRange {
                        start: step.start + step.delta,
                        end: r.end + step.delta,
                    });
                    if step.start > r.start {
                        r.end = step.start - 1; // new end of current range
                    } else {
                        range_covered = true;
                        break;
                    }
                } else if (step.start <= r.start) & (step.end >= r.end) {
                    // range 10..20, step 5..25
                    // => range 10..20 (+delta)
                    println!("case 3, break, {}..{} (+{})", r.start, r.end, step.delta,);
                    range_covered = true;
                    new_ranges.push(SeedRange {
                        start: r.start + step.delta,
                        end: r.end + step.delta,
                    });
                    break; // no need to check others steps, fully covered
                } else if (step.start > r.start) & (step.end < r.end) {
                    // range 10..20, step 12..18
                    // => 10..11, 12..18 (+delta), 19..20
                    new_ranges.push(SeedRange {
                        start: step.start + step.delta,
                        end: step.end + step.delta,
                    });
                    new_ranges.push(SeedRange {
                        start: r.start,
                        end: step.end - 1,
                    });
                    new_ranges.push(SeedRange {
                        start: r.end + 1,
                        end: r.end,
                    });
                    // r.end = step.start - 1;
                    // supposed to keep looking and replace range by 2 new ranges
                    // but can't do with this model :-(
                    range_covered = true;
                    break;
                } else {
                    panic!("uncovered scenario");
                }
                // println!("new {:?}", new_ranges);
            }
            if !range_covered {
                println!("case 4, unchanged {}..{}", r.start, r.end);
                new_ranges.push(SeedRange {
                    start: r.start,
                    end: r.end,
                });
            }
        }
        ranges = new_ranges;
        println!("end {:?}", ranges);
    }
    let mut min_val = i64::MAX;
    for r in ranges {
        min_val = min_val.min(r.start);
    }
    min_val
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    let mut all_lines = lines.lines();

    let Some(first) = all_lines.next() else {
        panic!("empty file")
    };
    let seeds: Vec<SeedRange> = parse_seeds(first);

    let mut maps: Vec<Vec<Map>> = vec![];
    for map in all_lines {
        if let Some(next) = map.chars().next() {
            if !next.is_ascii_digit() {
                // new step "x-to-y map:"
                maps.push(vec![]);
                continue;
            }
        } else {
            // empty, end of current step
            continue;
        }
        let map_op: Vec<&str> = map.split(' ').collect();
        let Ok(src) = map_op[1].parse::<i64>() else {
            panic!("expected src, found {}", map_op[1])
        };
        let Ok(dst) = map_op[0].parse::<i64>() else {
            panic!("expected dst, found {}", map_op[0])
        };
        let Ok(range) = map_op[2].parse::<i64>() else {
            panic!("expected range, found {}", map_op[2])
        };
        println!("insert {src}->{dst} ({range})");
        match maps.last_mut() {
            Some(step) => {
                step.push(Map {
                    start: src,         // 98
                    end: src + range,   // 92+2
                    delta: (dst - src), // 98-50
                });
            }
            _ => panic!("empty maps"),
        }
    }

    let mut min: i64 = i64::max_value();
    for range in seeds {
        println!("exploring range {:?}", range.start..=range.end);
        let score = find_path_range(
            SeedRange {
                start: range.start,
                end: range.end,
            },
            &maps,
        );
        min = min.min(score);
        println!("end of seed start {}, min {}", range.start, min);
    }
}

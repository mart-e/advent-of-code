use clap::{App, Arg};
use itertools::{rev, Itertools};
use std::fs;

fn extract_patterns(lines: String) -> Vec<Vec<String>> {
    lines
        .split("\n\n")
        .map(|p| p.lines().map(|l| l.to_string()).collect())
        .collect()
}

fn symetric_minus_one(lines: &Vec<String>) -> usize {
    let size = lines.len();
    for x in 0..size {
        if x > 0 {
            if x <= (size / 2) {
                if rev(lines[..x].to_vec())
                    .join("") // [0..x] in reverse, joined into a long string
                    .chars()
                    .zip(lines[x..x + x].to_vec().join("").chars()) // [x..block size], joined into a long sting
                    .filter(|(x, y)| x != y)
                    .count()  // only one char differ
                    == 1
                {
                    return x;
                }
            } else if x > (size / 2) {
                if rev(lines[x - (size - x)..x].to_vec())
                    .join("") // [block size..x] in reverse, joined into a long string
                    .chars()
                    .zip(lines[x..].to_vec().join("").chars()) // [x..end], joinded into a long string
                    .filter(|(x, y)| x != y)
                    .count()  // only one char differ
                    == 1
                {
                    return x;
                }
            }
        }
    }
    usize::MAX
}

fn find_reflection(pattern: &Vec<String>) -> (usize, usize) {
    // search vertically

    let x = symetric_minus_one(pattern);
    if x != usize::MAX {
        return (x, 0);
    }

    let hor_size = pattern.first().expect("misisng pattern").len();
    let mut ver: Vec<String> = vec!["".to_string(); hor_size];
    for line in pattern {
        for (idx, c) in line.chars().enumerate() {
            ver[idx].push(c);
        }
    }

    let y = symetric_minus_one(&ver);
    if y != usize::MAX {
        return (0, y);
    }

    (0, 0)
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut tot = 0;
    let res = extract_patterns(lines);
    for (idx, pattern) in res.iter().enumerate() {
        print!("Pattern {idx}: ");
        let (hor, ver) = find_reflection(pattern);
        println!("({hor}, {ver})");
        tot += ver;
        tot += 100 * hor;
    }

    println!("Tot: {tot}");

    if filename.contains("input") {
        assert_eq!(tot, 28627);
    } else if filename.contains("example") {
        assert_eq!(tot, 400);
    }

    println!("The end!");
}

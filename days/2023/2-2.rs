use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};
use regex::Regex;

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

struct Game {
    id: u32,
    blue: u32,
    red: u32,
    green: u32,
}

fn parse_file(reader: BufReader<File>) -> Vec<Game> {
    let re = Regex::new(r"^Game (?<id>\d*): (?<games>[\w ,;]*)$").unwrap(); // Game 42: ...
    let re_games = Regex::new(r"(?<game>[0-9 a-z,]*)").unwrap(); // 12 green, 13 red (not ;)
    let re_game = Regex::new(r"(?<num>\d*) (?<color>[a-z]*)").unwrap(); // 12 green
    let mut games: Vec<Game> = vec![];
    for line_ in reader.lines() {
        let line = line_.unwrap();
        if let Some(group) = re.captures(&line) {
            let id = &group["id"];
            let mut game = Game {
                id: id.parse::<u32>().unwrap(),
                blue: 0,
                red: 0,
                green: 0,
            };
            for (_, [g]) in re_games
                .captures_iter(&group["games"])
                .map(|cg| cg.extract())
            {
                for (_, [num, color]) in re_game.captures_iter(g).map(|c| c.extract()) {
                    if color.is_empty() {
                        continue;
                    }
                    if color == "blue" {
                        game.blue = game.blue.max(num.parse::<u32>().unwrap());
                    } else if color == "red" {
                        game.red = game.red.max(num.parse::<u32>().unwrap());
                    } else {
                        game.green = game.green.max(num.parse::<u32>().unwrap());
                    }
                }
            }
            println!(
                "... Result for game {}: red {}, green {}, blue {}",
                game.id, game.red, game.green, game.blue
            );
            games.push(game);
        }
    }

    games
}

fn power(game: &Game) -> u32 {
    game.red * game.blue * game.green
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: u32 = 0;
    for game in parse_file(reader) {
        sum += power(&game);
    }
    println!("Total power: {}", sum);
}

use std::{collections::HashMap, fs, hash::Hash};

use clap::{App, Arg};

#[derive(Eq, Hash, PartialEq, Clone, Debug)]
struct Coor {
    x: usize,
    y: usize,
}
const MAXC: Coor = Coor {
    x: usize::MAX,
    y: usize::MAX,
};

#[derive(Debug, Clone)]
struct Pipe {
    from: Coor,
    to: Coor,
    t: char,
}

fn map_lines(lines: String) -> (Coor, HashMap<Coor, Pipe>) {
    let mut map: HashMap<Coor, Pipe> = HashMap::new();
    let mut start: Coor = MAXC;
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let here = Coor { x, y };
            if let Some(p) = match c {
                'S' => {
                    start = here.clone();
                    None
                }
                '-' if x > 0 => Some(Pipe {
                    from: Coor { x: x - 1, y },
                    to: Coor { x: x + 1, y },
                    t: '-',
                }),
                '|' if y > 0 => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x, y: y + 1 },
                    t: '|',
                }),
                'L' if y > 0 => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x: x + 1, y },
                    t: 'L',
                }),
                'J' if (y > 0) & (x > 0) => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x: x - 1, y },
                    t: 'J',
                }),
                '7' if x > 0 => Some(Pipe {
                    from: Coor { x: x - 1, y },
                    to: Coor { x, y: y + 1 },
                    t: '7',
                }),
                'F' => Some(Pipe {
                    from: Coor { x: x + 1, y },
                    to: Coor { x, y: y + 1 },
                    t: 'F',
                }),
                _ => None, // . or overflow
            } {
                map.insert(here, p);
            }
        }
    }
    let mut to = MAXC;
    let mut from = MAXC;
    for (c, p) in map.clone().into_iter() {
        if (p.from == start) | (p.to == start) {
            if from != MAXC {
                to = c;
            } else {
                from = c
            }
        }
    }
    assert!(from != MAXC);
    assert!(to != MAXC);
    map.insert(start.clone(), Pipe { from, to, t: 'S' });
    (start, map)
}

fn travel_path(start: &Coor, map: &HashMap<Coor, Pipe>, go_left: bool) -> usize {
    let Some(mut p) = map.get(&start) else {
        panic!("starting pipe {:?} not found", &start);
    };
    let mut tot = 0;
    let mut next;
    let mut prev = start.clone();
    if go_left {
        next = p.to.clone();
    } else {
        next = p.from.clone();
    }
    while next != *start {
        match map.get(&next) {
            Some(new_p) => {
                if new_p.to != prev {
                    prev = next.clone();
                    next = new_p.to.clone();
                } else {
                    prev = next.clone();
                    next = new_p.from.clone();
                }
                println!(
                    "Traveling from {:?} to {:?}, found {:?} (step {})",
                    prev, next, new_p, tot,
                );
            }
            _ => panic!("dead end {:?}", p.to),
        }
        tot += 1;

        if tot > 100000 {
            panic!("too much, infinite loop?");
        }
    }
    println!("Back to start {:?} after {}", start, tot);
    tot
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let (start, map) = map_lines(lines);
    let res = travel_path(&start, &map, true);
    //travel_path(&start, &map, false); // useless
    println!("{}", (res - 1) / 2 + 1);
    println!("The end!");
}

use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

fn parse_matrix(reader: BufReader<File>) -> Vec<Vec<char>> {
    let mut lines: Vec<Vec<char>> = vec![];
    for line_ in reader.lines() {
        let line = line_.unwrap().clone();
        lines.push(line.chars().collect());
    }
    lines
}

fn check_symbol(lines: &[Vec<char>], x: usize, y: usize) -> bool {
    if (x >= lines.len()) || (y >= lines[x].len()) {
        // out of bound
        return false;
    }
    if (lines[x][y] != '.') & (!lines[x][y].is_ascii_digit()) {
        //println!("found symbol {} in position {}/{}", lines[x][y], x, y);
        return true;
    }
    false
}

fn find_numbers(lines: Vec<Vec<char>>) -> u32 {
    let mut sum: u32 = 0;
    for (x, line) in lines.iter().enumerate() {
        let mut part: u32 = 0;
        let mut symbol_present: bool = false;
        for (y, c) in line.iter().enumerate() {
            if c.is_ascii_digit() {
                //println!("found {c} at {x}-{y}");
                if part == 0 {
                    // start of a new part number
                    part = c.to_digit(10).unwrap();
                    // verify if a symbol is present
                    //  ?..
                    //  ?X.
                    //  ?..
                    if y > 0 {
                        symbol_present = symbol_present || check_symbol(&lines, x, y - 1);
                        symbol_present = symbol_present || check_symbol(&lines, x + 1, y - 1);
                    }
                    if (x > 0) & (y > 0) {
                        symbol_present = symbol_present || check_symbol(&lines, x - 1, y - 1);
                    }
                } else {
                    part = 10 * part + c.to_digit(10).unwrap();
                }
                //  .?.
                //  .X.
                //  .?.
                if ((x > 0) && check_symbol(&lines, x - 1, y)) | check_symbol(&lines, x + 1, y) {
                    symbol_present = true;
                }
                if (y == line.len() - 1) & (symbol_present) {
                    // part number at the end of the file
                    sum += part;
                }
            } else {
                if part > 0 {
                    // finish counting a part number
                    //  ..?
                    //  .X?
                    //  ..?
                    if ((x > 0) && check_symbol(&lines, x - 1, y))
                        | check_symbol(&lines, x, y)
                        | check_symbol(&lines, x + 1, y)
                    {
                        symbol_present = true;
                    }
                    if symbol_present {
                        sum += part;
                        println!("End of part {}, new sum {}", part, sum);
                    } else {
                        println!("Discarding part {}, still sum {}", part, sum);
                    }
                    part = 0;
                    symbol_present = false;
                }
            }
        }
    }
    sum
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let lines = parse_matrix(reader);
    let total = find_numbers(lines);

    println!("Total parts: {}", total);
}

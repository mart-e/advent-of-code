use std::collections::VecDeque;

use advent_of_code::get_lines;
use advent_of_code::map;
use advent_of_code::map::print_map;
use advent_of_code::map::Beam;
use advent_of_code::map::CMap;
use advent_of_code::map::Coord;

fn getc(dir: u8, c: &Coord, map: &CMap) -> Option<(Coord, char)> {
    if let Some(newc) = match dir {
        0 if c.y > 0 => Some(Coord::new(c.x, c.y - 1)),
        1 => Some(Coord::new(c.x + 1, c.y)),
        2 => Some(Coord::new(c.x, c.y + 1)),
        3 if c.x > 0 => Some(Coord::new(c.x - 1, c.y)),
        _ => None,
    } {
        if let Some(c) = map.get(&newc) {
            return Some((newc, *c));
        }
    }
    None
}

fn get_suc(beam: Beam, map: &CMap) -> Vec<Beam> {
    let &here = map.get(&beam.pos).unwrap();
    (0..=4)
        .filter(|&dir| ((4 + dir - beam.dir) % 4) != 2)
        .filter(|&dir| {
            (here == '.')
                || (here == '>' && dir == 1)
                || (here == 'v' && dir == 2)
                || (here == '<' && dir == 3)
                || (here == '^' && dir == 0)
        })
        .flat_map(|dir| {
            getc(dir, &beam.pos, map)
                .filter(|(next, c)| (*c != '#') && (!beam.prevs.contains(next)))
                .map(|(next, _)| beam.walk(next, dir))
        })
        .collect()
}

fn main() {
    let (filename, lines) = get_lines();

    let map = map::parse_map(lines);
    let start = Coord::new(1, 0);
    let end = Coord::new(
        map.clone().keys().max_by_key(|a| a.x).unwrap().x - 1,
        map.clone().keys().max_by_key(|a| a.y).unwrap().y,
    );

    //assert_eq!((start.x, start.y), (1, 0));
    if filename.contains("example") {
        assert_eq!((end.x, end.y), (21, 22));
    } else {
        assert_eq!((end.x, end.y), (139, 140));
    }

    let mut max_path = 0;
    let mut queue = VecDeque::from([Beam::new(start, 2)]);
    //let mut queue = VecDeque::from([Beam::new(Coord::new(4, 5), 1)]);
    while let Some(b) = queue.pop_front() {
        if b.pos == end {
            println!("Victory !");
            max_path = max_path.max(b.length);
            println!("new max: {}", max_path);
            print_map(&map, &b.prevs);
            continue;
        }
        let suc = get_suc(b.clone(), &map);
        //println!("{} -> {:?}", b.clone(), suc);
        queue.append(&mut VecDeque::from(suc));
    }
    let tot = max_path;
    println!("Tot: {tot}");
    println!("The end!");
}

use clap::{App, Arg};
use std::fs;

fn hash(step: &str) -> usize {
    let mut res = 0;
    for c in step.chars() {
        res += c as usize;
        res *= 17;
        res %= 256;
    }
    // step.chars()
    //     .map(|c| ((c as usize) * 17) % 256)
    //     .collect::<Vec<usize>>()
    //     .iter()
    //     .sum();
    res
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let steps: Vec<&str> = lines.trim().split(',').collect();

    let mut tot = 0;
    for step in steps {
        let res = hash(step);
        println!("{} becomes {}", step, res);
        tot += res;
    }

    if filename.contains("example") {
        assert_eq!(tot, 1320);
    } else if filename.contains("input") {
        assert_eq!(tot, 515210);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

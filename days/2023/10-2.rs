use std::{collections::HashMap, fs, hash::Hash};

use clap::{App, Arg};

#[derive(Eq, Hash, PartialEq, Clone, Debug)]
struct Coor {
    x: usize,
    y: usize,
}
const MAXC: Coor = Coor {
    x: usize::MAX,
    y: usize::MAX,
};

#[derive(Debug, Clone, PartialEq)]
struct Pipe {
    from: Coor,
    to: Coor,
    t: char,
}

fn map_lines(lines: String) -> (Coor, HashMap<Coor, Pipe>) {
    let mut map: HashMap<Coor, Pipe> = HashMap::new();
    let mut start: Coor = MAXC;
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let here = Coor { x, y };
            if let Some(p) = match c {
                'S' => {
                    start = here.clone();
                    None
                }
                '-' if x > 0 => Some(Pipe {
                    from: Coor { x: x - 1, y },
                    to: Coor { x: x + 1, y },
                    t: '-',
                }),
                '|' if y > 0 => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x, y: y + 1 },
                    t: '|',
                }),
                'L' if y > 0 => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x: x + 1, y },
                    t: 'L',
                }),
                'J' if (y > 0) & (x > 0) => Some(Pipe {
                    from: Coor { x, y: y - 1 },
                    to: Coor { x: x - 1, y },
                    t: 'J',
                }),
                '7' if x > 0 => Some(Pipe {
                    from: Coor { x: x - 1, y },
                    to: Coor { x, y: y + 1 },
                    t: '7',
                }),
                'F' => Some(Pipe {
                    from: Coor { x: x + 1, y },
                    to: Coor { x, y: y + 1 },
                    t: 'F',
                }),
                _ => None, // . or overflow
            } {
                map.insert(here, p);
            }
        }
    }
    let mut to = MAXC;
    let mut from = MAXC;
    for (c, p) in map.clone().into_iter() {
        if (p.from == start) | (p.to == start) {
            if from != MAXC {
                to = c;
            } else {
                from = c
            }
        }
    }
    assert!(from != MAXC);
    assert!(to != MAXC);
    let mut t = 'S';
    if from.x == to.x {
        t = '|'
    } else if from.y == to.y {
        t = '-';
    } else if (from.x < start.x) & (from.y == start.y) {
        // 7 or J
        assert_eq!(start.x, to.x);
        if to.y > start.y {
            t = '7';
        } else {
            t = 'J';
        }
    } else if (to.x < start.x) & (to.y == start.y) {
        // 7 or J
        assert_eq!(start.x, from.x);
        if from.y > start.y {
            t = '7';
        } else {
            t = 'J';
        }
    } else if (from.x > start.x) & (from.y == start.y) {
        // L or F
        if to.y > start.y {
            t = 'F';
        } else {
            t = 'L';
        }
    } else if (to.x > start.x) & (to.y == start.y) {
        // L or F
        if from.y > start.y {
            t = 'F';
        } else {
            t = 'L';
        }
    }
    map.insert(start.clone(), Pipe { from, to, t });
    (start, map)
}

fn travel_path(start: &Coor, map: &HashMap<Coor, Pipe>, go_left: bool) -> Vec<Pipe> {
    let Some(p) = map.get(&start) else {
        panic!("starting pipe {:?} not found", &start);
    };
    let mut tot = 0;
    let mut next;
    let mut prev = start.clone();
    if go_left {
        next = p.to.clone();
    } else {
        next = p.from.clone();
    }
    let mut visited: Vec<Pipe> = vec![];
    visited.push(p.clone());
    while next != *start {
        match map.get(&next) {
            Some(new_p) => {
                if new_p.to != prev {
                    prev = next.clone();
                    next = new_p.to.clone();
                } else {
                    prev = next.clone();
                    next = new_p.from.clone();
                }
                println!(
                    "Traveling from {:?} to {:?}, found {:?} (step {})",
                    prev, next, new_p, tot,
                );
                visited.push(new_p.clone());
            }
            _ => panic!("dead end {:?}", p.to),
        }
        tot += 1;

        if tot > 100000 {
            panic!("too much, infinite loop?");
        }
    }
    println!("Back to start {:?} after {}", start, tot);
    visited
}

fn fill_map(map: &HashMap<Coor, Pipe>, visited: &Vec<Pipe>) -> usize {
    let mut tot = 0;
    let max_y = map.keys().map(|c| c.y).max().unwrap();
    let max_x = map.keys().map(|c| c.x).max().unwrap();
    for y in 0..=max_y {
        let mut starting_pos = usize::MAX;
        let mut visited_count = 0;
        for x in 0..=max_x {
            let Some(p) = map.get(&Coor { x, y }) else {
                continue; // pipes out of map are not in map
            };
            if visited.contains(p) {
                match p.t {
                    '-' | 'J' | 'L' => {
                        if starting_pos != usize::MAX {
                            // exclude current tile from count
                            visited_count += 1;
                        }
                    }
                    'F' | '7' | '|' => {
                        // start or stop counting
                        if starting_pos == usize::MAX {
                            starting_pos = x;
                            visited_count = 0;
                        } else {
                            // end of area
                            starting_pos += 1; // count current pos
                            if x - starting_pos - visited_count > 0 {
                                println!(
                                    "end of area {}/{} to {}/{} (+{})",
                                    starting_pos,
                                    y,
                                    x,
                                    y,
                                    x - starting_pos - visited_count
                                );
                                tot += x - starting_pos - visited_count;
                            }
                            starting_pos = usize::MAX;
                        }
                    }
                    _ => panic!("unknow pipe {}", p.t),
                }
            }
        }
    }
    eprintln!("End with {}", tot);
    tot
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let (start, map) = map_lines(lines);
    let visited = travel_path(&start, &map, true);

    fill_map(&map, &visited);

    println!("The end!");
}

use std::{fs, str::Lines};

use clap::{App, Arg};
use itertools::Itertools;
use regex::Regex;
use std::collections::HashMap;

fn to_map(all: Lines) -> HashMap<String, (String, String)> {
    let mut map: HashMap<String, (String, String)> = HashMap::new();
    let Ok(re) = Regex::new(r"(?<src>\w*) = \((?<left>\w*), (?<right>\w*)\)") else {
        panic!("bad regex")
    };
    for line in all {
        if let Some(cap) = re.captures(line) {
            let src = &cap["src"];
            let left = &cap["left"];
            let right = &cap["right"];
            map.insert(src.to_owned(), (left.to_owned(), right.to_owned()));
        }
    }
    map
}

fn lcm(n1: usize, n2: usize) -> usize {
    let mut rem: usize = 0;
    let mut lcm: usize = 0;
    let mut x: usize = 0;
    let mut y: usize = 0;
    if n1 > n2 {
        x = n1;
        y = n2;
    } else {
        x = n2;
        y = n1;
    }

    rem = x % y;

    while (rem != 0) {
        x = y;
        y = rem;
        rem = x % y;
    }

    lcm = n1 * n2 / y;
    lcm
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut all = lines.lines();
    let Some(steps) = all.next() else {
        panic!("empty file");
    };
    let _ = all.next(); // empty line
    let instructions: HashMap<String, (String, String)> = to_map(all);

    let mut start: Vec<String> = vec![];
    for key in instructions.keys() {
        if (*key).ends_with('A') {
            start.push(key.clone());
        }
    }
    println!("start: {:?}", start);
    let mut tot: usize = 0;

    for mut pos in &start {
        tot = 0;
        let mut visited = vec![];
        let pos_init = pos;
        'outer: loop {
            for (n, step) in steps.chars().enumerate() {
                assert!((step == 'L') | (step == 'R'));
                if visited.contains(&(pos, n)) {
                    println!(
                        "Path {}: found visited node ({}, {}) after {} steps",
                        pos_init, pos, step, tot
                    );
                    //break 'outer;
                    //continue;
                }
                visited.push((pos, n));

                tot += 1;

                let Some((left, right)) = instructions.get(pos) else {
                    panic!("no instruction when at {}", pos)
                };
                if step == 'L' {
                    pos = left;
                } else {
                    pos = right;
                }
                if (*pos).ends_with('Z') {
                    println!("Path {}: found Z pos {} after {}", pos_init, pos, tot);
                    break 'outer;
                }
            }
        }
    }
    // compute LCM of all these numbers

    println!("The end!");
}

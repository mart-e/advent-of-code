use clap::{App, Arg};
use std::fs;

pub fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

pub mod map {
    use std::collections::{HashMap, HashSet};
    use std::fmt;
    use std::fmt::{Debug, Display};

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Coord {
        pub x: usize,
        pub y: usize,
    }
    impl Coord {
        pub fn new(x: usize, y: usize) -> Coord {
            Coord { x, y }
        }
    }
    impl Display for Coord {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            //write!(f, "{},{}", self.x, self.y)
            write!(f, "{},{}", self.y + 1, self.x)
        }
    }

    pub type CMap = HashMap<Coord, char>;
    pub fn parse_map(lines: String) -> CMap {
        lines
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .map(move |(x, c)| (Coord { x, y }, c))
            })
            .collect()
    }
    pub fn print_map(map: &CMap, visited: &HashSet<Coord>) {
        let Some(x_size) = map.keys().map(|c| c.x).max() else {
            panic!("bad map")
        };
        let Some(y_size) = map.keys().map(|c| c.y).max() else {
            panic!("bad map")
        };

        print!("\n");
        for x in 0..=x_size {
            print!("{}", x % 10);
        }
        print!("\n");
        for y in 0..=y_size {
            for x in 0..=x_size {
                let c = Coord { x, y };
                if visited.contains(&c) {
                    print!("\x1b[1;34mO\x1b[0;37m");
                } else {
                    let Some(c) = map.get(&c) else {
                        panic!("missig {}.{} in map", x, y)
                    };
                    match c {
                        '.' => print!("\x1b[1;38m.\x1b[0;37m"),
                        '#' => print!("\x1b[1;35m#\x1b[0;37m"),
                        _ => print!("\x1b[1;31m{}\x1b[0;37m", c),
                    };
                }
            }
            println!(" {y}");
        }
        println!("");
    }

    #[derive(PartialEq, Eq, Clone)]
    pub struct Beam {
        pub pos: Coord,
        pub dir: u8, // 0=N, 1=E, 2=S, 3=W
    }
    impl Beam {
        pub fn new(pos: Coord, dir: u8) -> Beam {
            assert!(dir <= 4);
            Beam { pos, dir }
        }
    }

    impl Display for Beam {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            if self.dir == 0 {
                write!(f, "{}↑", self.pos)
            } else if self.dir == 1 {
                write!(f, "{}→", self.pos)
            } else if self.dir == 2 {
                write!(f, "{}↓", self.pos)
            } else {
                write!(f, "{}←", self.pos)
            }
        }
    }
}

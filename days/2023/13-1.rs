use clap::{App, Arg};
use itertools::rev;
use std::fs;

fn extract_patterns(lines: String) -> Vec<Vec<String>> {
    lines
        .split("\n\n")
        .map(|p| p.lines().map(|l| l.to_string()).collect())
        .collect()
}

fn symetric(lines: &Vec<String>) -> usize {
    let size = lines.len();
    for x in 0..size {
        if (x > 0) && (lines[x] == lines[x - 1]) {
            if ((x <= (size / 2))
                && (rev(lines[..x].to_vec())
                    .zip(lines[x..x + x].to_vec())
                    .all(|(x, y)| x == y)))
                || ((x > (size / 2))
                    && (rev(lines[x - (size - x)..x].to_vec())
                        .zip(lines[x..].to_vec())
                        .all(|(x, y)| x == y)))
            {
                return x;
            }
        }
    }
    usize::MAX
}

fn find_reflection(pattern: &Vec<String>) -> (usize, usize) {
    // search vertically

    let x = symetric(pattern);
    if x != usize::MAX {
        return (x, 0);
    }

    let hor_size = pattern.first().expect("misisng pattern").len();
    let mut ver: Vec<String> = vec!["".to_string(); hor_size];
    for line in pattern {
        for (idx, c) in line.chars().enumerate() {
            ver[idx].push(c);
        }
    }

    let y = symetric(&ver);
    if y != usize::MAX {
        return (0, y);
    }

    (0, 0)
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut tot = 0;
    let res = extract_patterns(lines);
    for (idx, pattern) in res.iter().enumerate() {
        print!("Pattern {idx}: ");
        let (hor, ver) = find_reflection(pattern);
        println!("({hor}, {ver})");
        tot += ver;
        tot += 100 * hor;
    }

    println!("Tot: {tot}");

    if filename.contains("input") {
        assert_eq!(tot, 40006);
    } else if filename.contains("example") {
        assert_eq!(tot, 405);
    }

    println!("The end!");
}

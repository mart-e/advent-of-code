use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_lines(lines: String) -> Vec<(u32, u32)> {
    let mut all: Vec<Vec<u32>> = vec![];
    for line in lines.lines() {
        let mut vals: Vec<u32> = vec![];
        for step in line.split(' ') {
            if step.is_empty() {
                continue;
            }
            match step.parse::<u32>() {
                Ok(val) => vals.push(val),
                _ => continue,
            }
        }
        all.push(vals);
    }
    let Some(time) = all.first() else {
        panic!("time line not found")
    };
    let Some(distance) = all.iter().nth(1) else {
        panic!("distance line not found")
    };
    let mut res: Vec<(u32, u32)> = vec![];
    for (a, b) in time.iter().zip(distance.iter()) {
        res.push((*a, *b));
    }
    res
}

fn find_winning(time: u32, dist: u32) -> u32 {
    let mut wins = 0;
    for t in 0..=time {
        let remain = time - t;
        if remain * t > dist {
            println!("push {}, travel {} (goal {})", t, remain * t, dist);
            wins += 1;
        }
    }
    wins
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    let goals: Vec<(u32, u32)> = parse_lines(lines);
    println!("{:?}", goals);
    let mut total = 1;
    for (time, distance) in goals {
        let wins = find_winning(time, distance);
        if wins > 0 {
            total *= wins;
        }
    }
    println!("ending total {total}");
}

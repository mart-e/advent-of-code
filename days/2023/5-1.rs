use std::fs;
use std::fs::File; // access files
use std::io::BufReader;

use clap::{App, Arg};

#[allow(dead_code)]
fn get_file(filename: &str) -> BufReader<File> {
    let f = match File::open(filename) {
        Err(msg) => panic!("couldn't open {} ({})", filename, msg),
        Ok(file) => file,
    };
    BufReader::new(f)
}

fn parse_seeds(line: &str) -> Vec<i64> {
    let seeds = line.split(' ');
    let mut res: Vec<i64> = vec![];
    for seed in seeds {
        match seed.parse::<i64>() {
            Ok(seed_num) => res.push(seed_num),
            Err(_) => continue,
        }
    }
    res
}

struct Map {
    src: i64,
    delta: i64,
    range: i64,
}

fn find_path(mut seed: i64, maps: &Vec<Vec<Map>>) -> i64 {
    println!("searching for match for {}", seed);
    for vmap in maps {
        for step in vmap {
            if (step.src <= seed) & (step.src + step.range >= seed) {
                seed += step.delta;
                println!("possible step: {} + {} = {}", step.src, step.delta, seed);
                break;
            }
        }
        println!("no map found for seed {}", seed);
    }
    println!("end, got {}", seed);
    seed
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    let mut all_lines = lines.lines();

    let Some(first) = all_lines.next() else {
        panic!("empty file")
    };
    let seeds: Vec<i64> = parse_seeds(first);

    let mut maps: Vec<Vec<Map>> = vec![];
    for map in all_lines {
        if let Some(next) = map.chars().next() {
            if !next.is_ascii_digit() {
                // new step "x-to-y map:"
                maps.push(vec![]);
                continue;
            }
        } else {
            // empty, end of current step
            continue;
        }
        let map_op: Vec<&str> = map.split(' ').collect();
        let Ok(src) = map_op[1].parse::<i64>() else {
            panic!("expected src, found {}", map_op[1])
        };
        let Ok(dst) = map_op[0].parse::<i64>() else {
            panic!("expected dst, found {}", map_op[0])
        };
        let Ok(range) = map_op[2].parse::<i64>() else {
            panic!("expected range, found {}", map_op[2])
        };
        println!("insert {src}->{dst} ({range})");
        match maps.last_mut() {
            Some(step) => {
                step.push(Map {
                    src,
                    delta: (dst - src),
                    range,
                });
            }
            _ => panic!("empty maps"),
        }
    }

    let mut min: i64 = i64::max_value();
    for seed in seeds {
        let score = find_path(seed, &maps);
        min = min.min(score);
        println!("seed {} -> {} (min: {})", seed, score, min);
    }
}

use clap::{App, Arg};
use core::fmt;
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Coord {
    x: isize,
    y: isize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.x == isize::MAX {
            write!(f, "∞.{}", self.y)
        } else if self.y == isize::MAX {
            write!(f, "{}.∞", self.x)
        } else {
            write!(f, "{}.{}", self.x, self.y)
        }
    }
}

fn parse_instructions(lines: String) -> Vec<(String, isize)> {
    let mut res = vec![];
    for line in lines.lines() {
        let mut split = line.split_whitespace();
        let Some(dir) = split.next() else {
            panic!("Invalid line {line}")
        };
        let Some(count) = split.next() else {
            panic!("Invalid line {line}")
        };
        res.push((dir.to_string(), count.parse().unwrap()));
    }
    res
}

// dummy only usefull to draw a map
fn follow_instructions(instructions: &Vec<(String, isize)>) -> Vec<Coord> {
    let mut path: Vec<Coord> = vec![];
    let mut last: Coord = Coord { x: 0, y: 0 };
    path.push(last);
    for (dir, count) in instructions {
        println!("Going {} {} from {}", count, dir, last);
        for _ in 0..*count {
            match dir.as_str() {
                "U" => last.y -= 1,
                "D" => last.y += 1,
                "L" => last.x -= 1,
                "R" => last.x += 1,
                _ => panic!("unexepected dir {dir}"),
            }
            path.push(last);
        }
    }
    path
}

fn get_coord_from_instr(instructions: &Vec<(String, isize)>) -> (Vec<Coord>, isize) {
    let mut path: Vec<Coord> = vec![];
    let mut length = 0; // need the edge length for the pick theorem
    let mut last: Coord = Coord { x: 0, y: 0 };
    path.push(last);
    for (dir, count) in instructions {
        length += count;
        match dir.as_str() {
            "U" => last.y -= *count,
            "D" => last.y += *count,
            "L" => last.x -= *count,
            "R" => last.x += *count,
            _ => panic!("unexepected dir {dir}"),
        }
        path.push(last);
    }
    (path, length)
}

fn print_map(path: &Vec<Coord>) {
    let Some(x_max) = path.iter().map(|c| c.x).max() else {
        panic!("No x max")
    };
    let Some(x_min) = path.iter().map(|c| c.x).min() else {
        panic!("No x min")
    };
    let Some(y_max) = path.iter().map(|c| c.y).max() else {
        panic!("No y max")
    };
    let Some(y_min) = path.iter().map(|c| c.y).min() else {
        panic!("No y min")
    };
    println!(
        "Map from {} to {}",
        Coord { x: x_min, y: y_min },
        Coord { x: x_max, y: y_max }
    );

    for y in y_min..=y_max {
        for x in x_min..=x_max {
            if path.contains(&Coord { x, y }) {
                print!("\x1b[1;38m*\x1b[0;37m");
            } else {
                print!("\x1b[1;31m.\x1b[0;37m");
            }
        }
        println!(" {}", y);
    }
}

// triangle formula
// https://en.wikipedia.org/wiki/Shoelace_formula#Triangle_formula
fn area(points: Vec<Coord>) -> isize {
    let &first = points.first().unwrap();
    let mut tot = 0.0;
    let mut prev = first;
    for point in points {
        if prev != first {
            tot += ((prev.x * point.y) - (point.x * prev.y)) as f64;
        }
        prev = point;
    }
    // prev is the last one
    tot += ((prev.x * first.y) - (first.x * prev.y)) as f64;
    (tot * 0.5) as isize
}

// pick's theorem
// https://en.wikipedia.org/wiki/Pick%27s_theorem
fn count_points_inside(area: isize, edges: isize) -> isize {
    (area as f64 - (edges as f64) / 2.0 + 1.0) as isize
}

fn main() {
    let (filename, lines) = get_lines();

    // useless, first attempt, at least it draws a map
    let instructions = parse_instructions(lines);
    let path = follow_instructions(&instructions);
    print_map(&path);

    // thanks https://www.reddit.com/r/adventofcode/comments/18l0qtr/comment/kduw0c7/
    // for pointers to the right algorithms
    let (points, edges) = get_coord_from_instr(&instructions);
    let area = area(points);
    println!("Area: {}, edges: {}", area, edges);
    let tot = count_points_inside(area, edges) + edges;

    if filename.contains("example") {
        assert_eq!(tot, 62);
    } else if filename.contains("input") {
        assert_eq!(tot, 58550);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

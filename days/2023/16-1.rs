use clap::{App, Arg};
use core::fmt;
use std::collections::{HashMap, HashSet};
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Coord {
    x: usize,
    y: usize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}.{}", self.x, self.y)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Beam {
    pos: Coord,
    dir: u8, // 0=N, 1=E, 2=S, 3=W
}

impl Display for Beam {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.dir == 0 {
            write!(f, "{} ↑", self.pos)
        } else if self.dir == 1 {
            write!(f, "{} →", self.pos)
        } else if self.dir == 2 {
            write!(f, "{} ↓", self.pos)
        } else {
            write!(f, "{} ←", self.pos)
        }
    }
}

fn parse_map(lines: String) -> HashMap<Coord, char> {
    let mut map = HashMap::new();
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert(Coord { x, y }, c);
        }
    }

    map
}

fn travel(beam: Beam, map: &HashMap<Coord, char>, visited: &mut Vec<Beam>) {
    let next: Coord;
    if beam.pos.x == usize::MAX {
        next = Coord {
            x: 0,
            y: beam.pos.y,
        };
    } else if beam.pos.y == usize::MAX {
        next = Coord {
            x: beam.pos.x,
            y: 0,
        };
    } else {
        visited.push(beam.clone());
        next = match beam.dir {
            0 if beam.pos.y > 0 => Coord {
                x: beam.pos.x,
                y: beam.pos.y - 1,
            },
            1 => Coord {
                x: beam.pos.x + 1,
                y: beam.pos.y,
            },
            2 => Coord {
                x: beam.pos.x,
                y: beam.pos.y + 1,
            },
            3 if beam.pos.x > 0 => Coord {
                x: beam.pos.x - 1,
                y: beam.pos.y,
            },
            _ => return,
        };
    }
    if let Some(&tile) = map.get(&next) {
        //beam.pos = next;
        let mut new_dir = beam.dir;
        if (tile == '.')
            | ((tile == '|') & (beam.dir % 2 == 0))
            | ((tile == '-') & (beam.dir % 2 == 1))
        {
            let new_beam = Beam {
                pos: next.clone(),
                dir: new_dir,
            };
            if visited.contains(&new_beam) {
                println!("Already visited {}, skip", new_beam);
                return;
            }
            // | N S or - W E
            // unchanged, continue
            println!("travel {}", new_beam);
            travel(new_beam.clone(), map, visited);
        } else if tile == '/' {
            if beam.dir % 2 == 0 {
                // N -> 1, S -> 3
                new_dir = (new_dir + 4 + 1) % 4;
            } else {
                // W -> 2, E -> 1
                new_dir = (new_dir + 4 - 1) % 4;
            }
            let new_beam = Beam {
                pos: next.clone(),
                dir: new_dir,
            };
            if visited.contains(&new_beam) {
                println!("Already visited {}, skip", new_beam);
                return;
            }
            travel(new_beam, map, visited);
        } else if tile == '\\' {
            if beam.dir % 2 == 0 {
                // N -> 3, S -> 1
                new_dir = (new_dir + 4 - 1) % 4;
            } else {
                // W -> 0, E -> 2
                new_dir = (new_dir + 4 + 1) % 4;
            }
            let new_beam = Beam {
                pos: next.clone(),
                dir: new_dir,
            };
            if visited.contains(&new_beam) {
                println!("Already visited {}, skip", new_beam);
                return;
            }

            println!("travel {}", new_beam);
            travel(new_beam.clone(), map, visited);
        } else {
            // title - | but to split
            let new_beam = Beam {
                pos: next.clone(),
                dir: (new_dir + 3) % 4,
            };
            if visited.contains(&new_beam) {
                println!("Already visited {}, skip", new_beam);
                return;
            }
            println!("split travel {}", new_beam);
            travel(new_beam, map, visited);

            let new_beam = Beam {
                pos: next.clone(),
                dir: (new_dir + 1) % 4,
            };
            if visited.contains(&new_beam) {
                return;
            }
            println!("split travel-bis {}1", new_beam);
            travel(new_beam, map, visited);
        }
    }
    // Tile not found, out of map
}

fn main() {
    let (filename, lines) = get_lines();

    let map = parse_map(lines);

    let start = Coord {
        x: usize::MAX,
        y: 0,
    };
    let beam = Beam { pos: start, dir: 1 };
    let mut visited = vec![];

    travel(beam, &map, &mut visited);

    let pos = visited
        .iter()
        .map(|b| b.pos.clone())
        .collect::<HashSet<Coord>>();

    visited.sort_unstable_by_key(|b| b.pos.x + b.pos.y * 100);
    const MAP_SIZE: usize = 8;
    for y in 0..MAP_SIZE {
        for x in 0..MAP_SIZE {
            if pos.contains(&Coord { x, y }) {
                print!("\x1b[1;38m#\x1b[0;37m");
            } else {
                print!("\x1b[1;31m.\x1b[0;37m");
            }
        }
        print!("\n");
    }

    let tot = pos.len();
    if filename.contains("example") {
        assert_eq!(tot, 46);
    } else if filename.contains("input") {
        assert_eq!(tot, 7951);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

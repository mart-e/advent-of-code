use std::collections::HashSet;
use std::collections::VecDeque;

use advent_of_code::get_lines;
use advent_of_code::map;
use advent_of_code::map::Beam;
use advent_of_code::map::CMap;
use advent_of_code::map::Coord;
use itertools::Itertools;

fn getc(dir: u8, c: &Coord, map: &CMap) -> Option<(Coord, char)> {
    if let Some(newc) = match dir {
        0 if c.y > 0 => Some(Coord::new(c.x, c.y - 1)),
        1 => Some(Coord::new(c.x + 1, c.y)),
        2 => Some(Coord::new(c.x, c.y + 1)),
        3 if c.x > 0 => Some(Coord::new(c.x - 1, c.y)),
        _ => None,
    } {
        if let Some(c) = map.get(&newc) {
            return Some((newc, *c));
        }
    }
    None
}

fn intersections(map: &CMap) -> HashSet<&Coord> {
    map.iter()
        .filter(|(_, c)| **c != '#')
        .filter(|(coord, _)| {
            (0..=4)
                .flat_map(|dir| getc(dir, coord, map).filter(|(_, c)| *c != '#'))
                .collect_vec()
                .len()
                > 2
        })
        .map(|(coord, _)| coord)
        .collect()
}

fn get_suc(beam: Beam, map: &CMap) -> Vec<Beam> {
    //let &here = map.get(&beam.pos).unwrap();
    (0..=4)
        .filter(|&dir| ((4 + dir - beam.dir) % 4) != 2)
        .flat_map(|dir| {
            getc(dir, &beam.pos, map)
                //.filter(|(next, c)| (*c != '#') && (!beam.prevs.contains(next)))
                .filter(|(_, c)| *c != '#')
                .map(|(next, _)| Beam { pos: next, dir })
        })
        .collect()
}

fn main() {
    let (filename, lines) = get_lines();

    let map = map::parse_map(lines);
    let intersections = intersections(&map);
    println!("{:?}", intersections);

    let start = Coord::new(1, 0);
    let end = Coord::new(
        map.clone().keys().max_by_key(|a| a.x).unwrap().x - 1,
        map.clone().keys().max_by_key(|a| a.y).unwrap().y,
    );

    assert_eq!((start.x, start.y), (1, 0));
    if filename.contains("example") {
        assert_eq!((end.x, end.y), (21, 22));
    } else {
        assert_eq!((end.x, end.y), (139, 140));
    }

    let mut max_path = 0;
    let mut queue = VecDeque::from([(0, HashSet::new(), Beam::new(start, 2))]);

    while let Some((len, mut prev, b)) = queue.pop_front() {
        if b.pos == end {
            if len > max_path {
                max_path = len;
                println!("Victory ! {}", max_path);
            }
            continue;
        }

        // only check at intersections to reduce the size of prevs and minimize lookups
        if intersections.contains(&b.pos) {
            if prev.contains(&b.pos) {
                // already visited that intersection
                continue;
            } else {
                prev.insert(b.clone().pos);
            }
        }

        let suc = get_suc(b.clone(), &map);
        suc.into_iter()
            .for_each(|b| queue.push_front((len + 1, prev.clone(), b)));
    }
    let tot = max_path;
    println!("Tot: {tot}");
    println!("The end!");
}

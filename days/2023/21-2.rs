use clap::{App, Arg};
use core::fmt;
use itertools::Itertools;
use std::collections::{HashMap, VecDeque};
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

const MAP_SIZE: usize = 131;
const TOT_STEPS: usize = 26501365;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Coord {
    x: usize,
    y: usize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}.{}", self.x, self.y)
    }
}
impl Coord {
    // 0 N, 1 E, 2 S, 3 W
    fn walk(&self, dir: u8, map: &GardenMap) -> Option<Coord> {
        let new = match dir {
            0 if self.y > 0 => Coord {
                x: self.x,
                y: self.y - 1,
            },
            1 if self.x < MAP_SIZE - 1 => Coord {
                x: self.x + 1,
                y: self.y,
            },
            2 if self.y < MAP_SIZE - 1 => Coord {
                x: self.x,
                y: self.y + 1,
            },
            3 if self.x > 0 => Coord {
                x: self.x - 1,
                y: self.y,
            },
            _ => return None,
        };
        match *map.get(&new).unwrap() {
            Tile::Rock => None,
            _ => Some(new),
        }
    }
}

#[derive(Clone)]
struct DirCoord {
    c: Coord,
    dir: u8, // 0 N, 1 E, 2 S, 3 W
}
impl PartialEq for DirCoord {
    fn eq(&self, other: &Self) -> bool {
        self.c == other.c
    }
}
impl fmt::Debug for DirCoord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.dir {
            0 => write!(f, "{}↑", self.c),
            1 => write!(f, "{}→", self.c),
            2 => write!(f, "{}↓", self.c),
            3 => write!(f, "{}←", self.c),
            _ => write!(f, "{}*", self.c),
        }
    }
}

#[derive(Clone)]
enum Tile {
    Start,
    Plot,
    Rock,
}

type GardenMap = HashMap<Coord, Tile>;

fn parse_map(lines: String) -> GardenMap {
    let mut map: GardenMap = HashMap::new();
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            let v = match c {
                '.' => Tile::Plot,
                'S' => Tile::Start,
                '#' => Tile::Rock,
                _ => panic!("unknown"),
            };
            map.insert(Coord { x, y }, v);
        }
    }
    map
}

fn print_map(map: &GardenMap, pos: &Vec<Coord>) {
    let Some(x_size) = map.keys().map(|c| c.x).max() else {
        panic!("bad map")
    };
    let Some(y_size) = map.keys().map(|c| c.y).max() else {
        panic!("bad map")
    };

    print!("\n");
    for x in 0..=x_size {
        print!("{}", x % 10);
    }
    print!("\n");

    for y in 0..=y_size {
        for x in 0..=x_size {
            let c = Coord { x, y };
            if pos.contains(&c) {
                print!("\x1b[1;34mO\x1b[0;37m");
            } else {
                let Some(t) = map.get(&c) else {
                    panic!("missig {}.{} in map", x, y)
                };
                match t {
                    Tile::Plot => print!("\x1b[1;38m.\x1b[0;37m"),
                    Tile::Rock => print!("\x1b[1;35m#\x1b[0;37m"),
                    Tile::Start => print!("\x1b[1;31mS\x1b[0;37m"),
                };
            }
        }
        println!(" {y}");
    }
    println!("");
}

fn walk(map: &GardenMap, pos: &Coord) -> Vec<Coord> {
    (0..=3)
        .map(|dir| pos.walk(dir, map))
        .flatten()
        .collect_vec()
}

fn main() {
    let (_, lines) = get_lines();

    let map = parse_map(lines);
    let Some(start) = map
        .clone()
        .into_iter()
        .filter_map(|(c, t)| match t {
            Tile::Start => Some(c),
            _ => None,
        })
        .next()
    else {
        panic!("No start found");
    };

    let mut edges: VecDeque<(usize, Coord)> = VecDeque::from([(0, start)]);
    print_map(&map, &vec![]);

    let mut distances: Vec<usize> = vec![];
    let mut visited = HashMap::new();
    while let Some((dist, coord)) = edges.pop_front() {
        let new_pos = walk(&map, &coord);

        // identify edges, newly visited coordinates
        let new_edges: Vec<Coord> = new_pos
            .into_iter()
            .filter(|p| !visited.contains_key(p))
            .collect();

        //visited.append(&mut new_edges.to_vec());
        new_edges.into_iter().for_each(|pos| {
            distances.push(dist + 1);
            visited.insert(pos, dist + 1);
            edges.push_back((dist + 1, pos));
        });
    }
    // number of steps we can reach in max 64 steps
    let answer1 = distances
        .clone()
        .into_iter()
        .filter(|dist| *dist <= 64 && *dist % 2 == 0)
        .count();
    println!("Answer to part 1: {answer1}");

    // (65 steps from centers + 131 for each square
    let nsquares = (TOT_STEPS - (MAP_SIZE / 2)) / MAP_SIZE;
    assert_eq!(nsquares, 202300);

    println!(
        "When making {} steps from the center, will traverse {} squares",
        TOT_STEPS, nsquares
    );

    // cf https://github.com/villuna/aoc23/wiki/A-Geometric-solution-to-advent-of-code-2023,-day-21
    // number of even and odd squares
    // e.g. if nsquares = 1 -> 1 odd, 4 even
    //  O
    // OEO
    //  O
    let even = nsquares * nsquares;
    let odd = (nsquares + 1) * (nsquares + 1);
    println!(
        "This will make a diamond shape with {} + {} squares",
        even, odd
    );

    let odd_per_square = distances
        .clone()
        .into_iter()
        .filter(|dist| dist % 2 == 1)
        .count();
    let even_per_square = distances
        .clone()
        .into_iter()
        .filter(|dist| dist % 2 == 0)
        .count();
    println!(
        "In a square, we can visit {} tile in odd squares and {} in even ones",
        odd_per_square, even_per_square
    );

    let odd_corners = distances
        .clone()
        .into_iter()
        .filter(|&dist| dist % 2 == 1 && dist > 65)
        .count();
    let even_corners = distances
        .clone()
        .into_iter()
        .filter(|&dist| dist % 2 == 0 && dist > 65)
        .count();

    println!("In addition to those, we have to add {} corners of {} tiles (even) and remove {} corners of {} tiles (odd)", nsquares, even_corners, nsquares+1, odd_corners);
    let answer2 = odd * odd_per_square + even * even_per_square - ((nsquares + 1) * odd_corners)
        + (nsquares * even_corners);
    // 623540829615589
    println!("That makes a total of {}", answer2);

    println!("The end!");
}

use clap::{App, Arg};
use core::fmt;
use pathfinding::prelude::dijkstra;
use std::collections::HashMap;
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Coord {
    x: usize,
    y: usize,
}
impl Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.x == usize::MAX {
            write!(f, "∞.{}", self.y)
        } else if self.y == usize::MAX {
            write!(f, "{}.∞", self.x)
        } else {
            write!(f, "{}.{}", self.x, self.y)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
struct Beam {
    pos: Coord,
    dir: u8,  // 0=N, 1=E, 2=S, 3=W
    turn: u8, // how many since last turn
}

impl Display for Beam {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.dir == 0 {
            write!(f, "{}↑", self.pos)
        } else if self.dir == 1 {
            write!(f, "{}→", self.pos)
        } else if self.dir == 2 {
            write!(f, "{}↓", self.pos)
        } else {
            write!(f, "{}←", self.pos)
        }
    }
}

fn parse_map(lines: String) -> HashMap<Coord, usize> {
    let mut map = HashMap::new();
    for (y, line) in lines.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert(Coord { x, y }, c.to_string().parse().unwrap());
        }
    }
    map
}

fn print_map(map: &HashMap<Coord, usize>, path: &Vec<Beam>) {
    let Some(x_size) = map.keys().map(|c| c.x).max() else {
        panic!("bad map")
    };
    let Some(y_size) = map.keys().map(|c| c.y).max() else {
        panic!("bad map")
    };

    print!("\n");
    for x in 0..=x_size {
        print!("{}", x % 10);
    }
    print!("\n");

    for y in 0..=y_size {
        for x in 0..=x_size {
            let Some(c) = map.get(&Coord { x, y }) else {
                panic!("missig {}.{} in map", x, y)
            };
            if let Some(b) = path.iter().find(|b| b.pos == Coord { x, y }) {
                match b.dir {
                    0 => print!("^"),
                    1 => print!(">"),
                    2 => print!("v"),
                    _ => print!("<"),
                }
            } else {
                print!("\x1b[1;3{}m{}\x1b[0;37m", c, c);
            }
        }
        println!(" {y}");
    }
    println!("");
}

// from starting beam and direction, return a new beam if possible
fn add_successor(prev: &Beam, dir: u8, min_consec: u8, max_consec: u8) -> Vec<Beam> {
    if (dir != prev.dir) & ((dir + prev.dir) % 2 == 0) {
        // backward
        return vec![];
    }
    let turn: u8;
    if dir == prev.dir {
        if prev.turn >= max_consec {
            // would go over maximum, must turn
            return vec![];
        }
        turn = prev.turn + 1;
    } else {
        if prev.turn < min_consec {
            // turn too early
            return vec![];
        };
        turn = 1;
    }
    match dir {
        0 => vec![Beam {
            pos: Coord {
                x: prev.pos.x,
                y: prev.pos.y - 1,
            },
            dir,
            turn,
        }],
        1 => vec![Beam {
            pos: Coord {
                x: prev.pos.x + 1,
                y: prev.pos.y,
            },
            dir,
            turn,
        }],
        2 => vec![Beam {
            pos: Coord {
                x: prev.pos.x,
                y: prev.pos.y + 1,
            },
            dir,
            turn,
        }],
        3 => vec![Beam {
            pos: Coord {
                x: prev.pos.x - 1,
                y: prev.pos.y,
            },
            dir,
            turn,
        }],
        _ => panic!("unknow direction {dir}"),
    }
}

// list all possible successors from a beam, including cost
fn get_successors(
    beam: &Beam,
    map: &HashMap<Coord, usize>,
    min_consec: u8,
    max_consec: u8,
) -> Vec<(Beam, usize)> {
    let mut res: Vec<(Beam, usize)> = vec![];
    let pos = beam.pos;
    let mut nexts: Vec<Beam> = vec![];

    nexts.append(&mut add_successor(beam, 1, min_consec, max_consec)); // E
    nexts.append(&mut add_successor(beam, 2, min_consec, max_consec)); // S

    if pos.x > 0 {
        nexts.append(&mut add_successor(beam, 3, min_consec, max_consec)); // W
    }
    if pos.y > 0 {
        nexts.append(&mut add_successor(beam, 0, min_consec, max_consec)); // N
    }
    for next in nexts {
        let Some(&cost) = map.get(&next.pos) else {
            // out of bound
            continue;
        };
        res.push((next, cost));
    }
    res
}

fn main() {
    let (_, lines) = get_lines();

    let map = parse_map(lines);
    print_map(&map, &vec![]);
    let max_x = map.keys().map(|c| c.x).max().unwrap();
    let max_y = map.keys().map(|c| c.y).max().unwrap();

    let start: Beam = Beam {
        pos: Coord { x: 0, y: 0 },
        dir: 1,
        turn: 1,
    };
    let GOAL: Coord = Coord { x: max_x, y: max_y };

    let Some((result, cost)) = dijkstra(
        &start,
        |b| get_successors(b, &map, 4, 10),
        |b| (b.pos == GOAL) & (b.turn >= 4),
    ) else {
        panic!("no path found")
    };
    for b in &result {
        print!(" {b} ");
    }
    print!("\n");
    print_map(&map, &result);

    println!("Tot: {cost}");

    println!("The end!");
}

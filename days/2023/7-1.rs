use std::fs;

use clap::{App, Arg};
use itertools::Itertools;
use std::collections::HashMap;

const CARDS: [char; 14] = [
    'A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2', '1',
];

fn card_value(c: char) -> usize {
    let Some(pos) = CARDS.iter().rev().position(|&x| x == c) else {
        panic!("{} not a card", c)
    };
    pos + 1
}

fn parse_lines(lines: &str) -> Vec<(&str, usize)> {
    let mut all: Vec<(&str, usize)> = vec![];
    for line in lines.lines() {
        let mut split = line.split_whitespace();
        let Some(hand) = split.next() else {
            panic!("missing hand on line")
        };
        let Some(bid) = split.next() else {
            panic!("missing bid on line")
        };
        let Ok(bid_u) = bid.parse::<usize>() else {
            panic!("trying to parse bid {}", bid)
        };
        all.push((hand, bid_u));
    }
    all
}

// 7 = five, 6 = four, 5 = full house,...
fn hand_score(hand: &str) -> u8 {
    assert_eq!(hand.len(), 5);
    //let mut cs: Vec<char> = hand.chars().collect();
    let groups: HashMap<char, u32> = hand
        .chars()
        .into_group_map_by(|&x| x)
        .into_iter()
        .map(|(k, v)| (k, v.len() as u32))
        .collect();

    if groups.len() == 1 {
        return 7; // five of a kind
    } else if groups.len() == 2 {
        if groups.iter().any(|(_, v)| *v == 4) {
            return 6; // four of a kind
        } else {
            return 5; // full house
        }
    } else if groups.len() == 3 {
        if groups.iter().any(|(_, v)| *v == 3) {
            return 4; // three of a kind
        } else {
            return 3; // two pair
        }
    } else if groups.len() == 4 {
        return 2; // one pair
    }
    1 // hight card
}

fn hand_value(hand: &str) -> usize {
    let mut res = hand_score(hand) as usize * 10000000000;
    for (x, c) in hand.chars().rev().enumerate() {
        res += (100_usize.pow(x.try_into().unwrap())).max(1) * card_value(c);
    }
    res
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let mut res = parse_lines(&lines);
    res.sort_by(|(ls, _lb), (rs, _)| hand_value(ls).partial_cmp(&hand_value(rs)).unwrap());
    let mut tot = 0;
    for (x, (hand, bid)) in res.iter().enumerate() {
        println!("#{}: {} bid {} : +{}", x + 1, hand, bid, (x + 1) * bid);
        tot += (x + 1) * bid;
    }
    println!("{tot}");

    println!("The end!");
}

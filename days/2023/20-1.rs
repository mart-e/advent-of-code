use clap::{App, Arg};
use core::fmt;

use std::collections::{HashMap, VecDeque};
use std::fmt::Display;
use std::fs;

fn get_lines() -> (String, String) {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };
    (filename.to_string(), lines)
}

// stolen from https://github.com/cainkellye/advent_of_code/blob/main/src/y2023/day20.rs
fn name_to_id(name: String) -> usize {
    if name == "broadcaster" {
        return 0;
    }
    let mut id = 0;
    for (exp, byte) in name.bytes().enumerate() {
        id += (byte as usize) * 256_usize.pow(exp as u32);
    }
    println!("name_to_id {} = {}", name, id);
    id
}

fn id_to_name(id: usize) -> String {
    if id == 0 {
        return "broadcaster".to_string();
    }
    "other".to_string()
}

fn parse_destinations(lines: String) -> (ModuleMap, LinksMap) {
    let mut map: ModuleMap = HashMap::new();
    let mut connections: LinksMap = HashMap::new();

    // first time, parse list of buttons
    for line in lines.clone().lines() {
        let (left, _) = line.split_once(" -> ").unwrap();
        match left.chars().next().unwrap() {
            '%' => {
                let name = left[1..].to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(FlipFlop {
                        id: name_to_id(name.clone()),
                        name,
                        last: false,
                    }),
                );
            }
            '&' => {
                let name = left[1..].to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(Conjonction {
                        id: name_to_id(name.clone()),
                        name,
                        inputs: HashMap::new(),
                    }),
                );
            }
            _ => {
                let name = left.to_string();
                map.insert(
                    name_to_id(name.clone()),
                    Box::new(Broadcast {
                        id: name_to_id(name.clone()),
                        name,
                    }),
                );
            }
        };
    }

    // second time, make connections
    for line in lines.clone().lines() {
        let (left, right) = line.split_once(" -> ").unwrap();
        let src = match left.chars().next().unwrap() {
            '%' | '&' => name_to_id(left[1..].to_string()),
            _ => name_to_id(left.to_string()),
        };

        let v: Vec<usize> = right
            .split(',')
            .map(|n| name_to_id(n.trim().to_string()))
            .collect();
        connections.insert(src, v);
    }

    // find input connections for each module
    for (mid, module) in &mut map {
        module.connect(
            connections
                .clone()
                .into_iter()
                .filter(|(_, dst)| dst.contains(&mid))
                .map(|(src, _)| src)
                .collect(),
        );
    }

    (map, connections)
}

// to retrieve a Module from his number
type ModuleMap = HashMap<usize, Box<dyn Module>>;
// to retrieve the incoming/outgoing links from a module number
type LinksMap = HashMap<usize, Vec<usize>>;

trait Module {
    fn receive(&mut self, from: usize, high: bool) -> Option<bool>;
    fn connect(&mut self, inputs: Vec<usize>);
    fn name(&self) -> String;
}

struct Broadcast {
    id: usize,
    name: String,
}
impl Module for Broadcast {
    fn receive(&mut self, _: usize, high: bool) -> Option<bool> {
        Some(high)
    }
    fn connect(&mut self, _: Vec<usize>) {}
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct FlipFlop {
    id: usize,
    name: String,
    last: bool,
}
impl Module for FlipFlop {
    fn receive(&mut self, from: usize, high: bool) -> Option<bool> {
        if high {
            None
        } else {
            self.last = !self.last;
            Some(self.last)
        }
    }
    fn connect(&mut self, inputs: Vec<usize>) {}
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct Conjonction {
    id: usize,
    name: String,
    inputs: HashMap<usize, bool>,
}
impl Module for Conjonction {
    fn receive(&mut self, from: usize, high: bool) -> Option<bool> {
        // update last pulse
        self.inputs.entry(from).and_modify(|v| *v = high);
        if self.inputs.iter().all(|(_, &inp)| inp) {
            // all inputs are high -> return low
            Some(false)
        } else {
            Some(true)
        }
    }
    fn connect(&mut self, inputs: Vec<usize>) {
        inputs.into_iter().for_each(|input| {
            self.inputs.insert(input, false);
        })
    }
    fn name(&self) -> String {
        self.name.clone()
    }
}

struct Message {
    src: usize,
    dst: usize,
    high: bool,
}

impl Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.high {
            write!(f, "{} -high-> {}", self.src, self.dst)
        } else {
            write!(f, "{} -low-> {}", self.src, self.dst)
        }
    }
}

fn send_pulse(message: Message, connections: &LinksMap, map: &mut ModuleMap) -> Vec<Message> {
    println!("{message}");
    let Some(module) = map.get_mut(&message.dst) else {
        return vec![];
        //panic!("No module {} found", message.dst);
    };
    if let Some(pulse) = module.receive(message.src, message.high) {
        let Some(r) = connections.get(&message.dst) else {
            panic!("No connection found for {}", message.dst);
        };
        return r
            .iter()
            .map(|&n| Message {
                src: message.dst,
                dst: n,
                high: pulse,
            })
            .collect();
    }
    vec![]
}

fn main() {
    let (filename, lines) = get_lines();

    let (mut map, connections) = parse_destinations(lines);
    let mut low_count = 0;
    let mut high_count = 0;

    for _ in 0..1000 {
        let mut messages: VecDeque<Message> = VecDeque::from([Message {
            src: 0,
            dst: name_to_id("broadcaster".to_string()),
            high: false,
        }]);
        while !messages.is_empty() {
            let me = messages.pop_front().expect("empty messages");
            if me.high {
                high_count += 1;
            } else {
                low_count += 1;
            }
            send_pulse(me, &connections, &mut map)
                .into_iter()
                .for_each(|m| messages.push_back(m));
        }
    }

    println!("Low: {low_count}");
    println!("High: {high_count}");
    let tot: usize = low_count * high_count;
    if filename.contains("example") {
        assert_eq!(tot, 32000000);
    } else if filename.contains("input") {
        assert_eq!(tot, 896998430);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

use clap::{App, Arg};
use std::{array, collections::HashMap, fs};

fn hash(step: &str) -> usize {
    let mut res = 0;
    for c in step.chars() {
        res += c as usize;
        res *= 17;
        res %= 256;
    }
    // step.chars()
    //     .map(|c| ((c as usize) * 17) % 256)
    //     .collect::<Vec<usize>>()
    //     .iter()
    //     .sum();
    res
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();

    let Some(filename) = args.value_of("filename") else {
        panic!("missing filename argument");
    };

    let Ok(lines) = fs::read_to_string(filename) else {
        panic!("can't read file {}", filename)
    };

    let steps: Vec<&str> = lines.trim().split(',').collect();
    let mut map: [Vec<(&str, usize)>; 256] = array::from_fn(|_| vec![]);
    let mut hmap: HashMap<&str, (usize, usize)> = HashMap::new();

    for (idx, &step) in steps.iter().enumerate() {
        if step.chars().last().expect("empty string") == '-' {
            // remove if exists
            let boxn = &step[..step.len() - 1];
            hmap.remove(boxn);
            // remove if exists
            map[hash(boxn)] = map[hash(boxn)]
                .iter()
                .filter(|(n, _)| n != &boxn)
                .cloned()
                .collect::<Vec<(&str, usize)>>();
        } else {
            let Some((boxn, val)) = step.split_once('=') else {
                panic!("Wrong step {step}")
            };

            let uval = val.parse::<usize>().expect("{val} is not usize");
            if let Some((_, e)) = hmap.get_mut(boxn) {
                // exists, replace value
                *e = uval;
                map[hash(boxn)] = map[hash(boxn)]
                    .iter()
                    .map(|(n, v)| if n != &boxn { (*n, *v) } else { (*n, uval) })
                    .collect::<Vec<(&str, usize)>>();
            } else {
                hmap.insert(boxn, (idx, uval));
                map[hash(boxn)].push((boxn, uval));
            }
        }
    }
    println!("{:?}", map);

    let mut tot = 0;
    for (box_idx, boxv) in map.iter().enumerate() {
        if boxv.is_empty() {
            continue;
        }
        let box_tot: usize = boxv
            .iter()
            .enumerate()
            .map(|(i, (_, v))| (i + 1) * v)
            .collect::<Vec<usize>>()
            .iter()
            .sum();
        println!("{} * {}", box_tot, box_idx + 1);
        tot += box_tot * (box_idx + 1);
    }

    if filename.contains("example") {
        assert_eq!(tot, 145);
    } else if filename.contains("input") {
        assert_eq!(tot, 246762);
    }

    println!("Tot: {tot}");
    println!("The end!");
}

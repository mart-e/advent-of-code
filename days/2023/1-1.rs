use std::fs::File; // access files
use std::io::prelude::*;
use std::io::BufReader;

use clap::{App, Arg};

fn get_file(filename: &str) -> BufReader<File> {
    let f = File::open(filename).unwrap();
    BufReader::new(f)
}

fn main() {
    let args = App::new("advent-of-code")
        .version("2023")
        .arg(Arg::with_name("filename").required(true))
        .get_matches();
    let filename = args.value_of("filename").unwrap();
    let reader = get_file(filename);

    let mut sum: u32 = 0;
    for line_ in reader.lines() {
        let line = line_.unwrap();
        let mut first: u32 = 0;
        let mut last: u32 = 0;
        for c in line.chars() {
            if c.is_ascii_digit() {
                if first == 0 {
                    first = c.to_digit(10).unwrap();
                }
                // always replace last one with next occurrence
                last = c.to_digit(10).unwrap();
            }
        }
        // not match found, probably an issue
        assert_ne!(first, 0);

        println!("{} {} + {}", line, sum, first * 10 + last);
        sum += first * 10;
        sum += last;
    }
    println!("Sum of all of the calibration values: {}", sum);
}
